<?php
/**
 * Spo handling
 * @author redlobster
 */


namespace Finder;

class FinderSpo extends FinderRealmHandler{
    
    /**
     * Compliance of action and method
     * @param string $action
     * @return string|boolean
     */
    public function route($action){
        
        switch($action){
            
            case 'spo_countries':
                return 'getCountries';
            break;   
        
            case 'spo_list':
                return 'getList';
            break;   
                 
            case 'spo_countries':
                return 'getCountries';
            break;
        
            case 'book_spo':
                return 'book';
            break;
        
            case 'book_spo_check':
                return 'checkBook';
            break;
            case 'book_spo_data':
                return 'getBookingData';      
            break;
            case 'meal':
                return 'getMeal';
            break;
                
        }
        
        return false;
    }           
    
    
    /**
     * Available countries
     * @return array
     */
    public function getCountries(){
        $sql = ' 
            SELECT DISTINCT country.code as key, country.name
            FROM special
            INNER JOIN country ON country.id = special.country_id
            ORDER BY country.name ';
        return $this->handler->getRtList($sql, false);          
    }
    
    
    /**
     * List of spo for selected country
     * @return array
     */
    public function getList(){
        $list = array();
        if(!empty($this->params['country'])){        
            $query='
               SELECT 
                   special.id, 
                   special.nametour AS name,                   
                   special.price, 
                   special.fileprice AS file, 
                   special.date_create AS dcreate, 
                   special.date_begin AS begin, 
                   special.date_end AS end, 
                   special.offer_type AS tip,
                   special.valute AS rate,
                   city.name AS town                 
               FROM special 
               INNER JOIN country ON country.id = special.country_id 
               INNER JOIN city ON city.id = special.city_id 
               WHERE country.code IN (\''.strtoupper($this->params['country']).'\', \''.strtolower($this->params['country']).'\')';
            $list = $this->handler->getRtList($query, false);           
            if($list) foreach($list as $k => $v){
                $begin = strtotime($v['begin']);
                $list[$k]['begin'] = date('d.m.Y', $begin);
                $end = strtotime($v['end']);
                $list[$k]['end'] = date('d.m.Y', $end);
                $list[$k]['rate'] = preg_replace(array('/\$/','/EU$/'), array('USD','EUR'),$v['rate']);
            }
        }
        return $list;
    }
    
    
    /**
     * Available meal types
     * @return array
     */
    public function getMeal(){
        $sql = 'SELECT id, code,name,alias,description FROM food WHERE oper_id=4 AND id>0 ORDER BY code,name ASC LIMIT 20';
        $result = $this->handler->getRtList($sql, false);
        return $result;
        
    }
    
    /**
     * Booking spo offer
     * @return array
     */
    public function book(){
        $spo_data_string = $this->params['spo'];
        $data = json_decode($spo_data_string, true);
        $hash = md5($spo_data_string);
        $this->handler->setCache('spo_'.$hash, $data);
        
        $url = 'http://online-rosstour.ru/searchForm/process/constructor.php?';        
        $url = FinderUtilities::paramsToUrl($url, array('spo_hash' => $hash)); 
        FinderUtilities::getDataByCurl($url);   
        return ['hash' => $hash];        
    }
    
    
    /**
     * Checking data of application
     * @return array
     * @throws \Exception
     */
    public function checkBook(){
        if(empty($this->params['hash'])) throw new \Exception ('Hash parameter nedeed.');     
        return $this->handler->getCache('spo_'.$this->params['hash']);
    }
    
    
    /**
     * Getting application data during booking proccess. Available once.
     * @return array
     * @throws \Exception
     */
    public function getBookingData(){
        if(empty($this->params['hash'])) throw new \Exception ('Hash parameter nedeed.');        
        $hash = $this->params['hash'];
        $data = $this->handler->getCache('spo_'.$hash);
        $this->handler->setCache('spo_'.$hash, null);
        return $data;
    }    
}
