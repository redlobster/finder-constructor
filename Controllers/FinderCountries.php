<?php

namespace Finder;

class FinderCountries extends FinderRealmHandler{
    
    /**    
    * @var string
    * @access protected
    */
    protected $data_url = 'http://rt.ross-tur.ru/jobs/extgeo.php?action=citycountry';
    
    /**    
    * @var string
    * @access protected
    */
    protected $country_data_url = 'http://rt.ross-tur.ru/constructor/1/?action=getTownAndAirport&country=';    
    
    /**    
    * @var array
    * @access protected
    */
    protected $countries = array(
                                'au' => 'Австралия',
                                'at' => 'Австрия',
                                'az' => 'Азербайджан',
                                'ax' => 'Аландские острова',
                                'al' => 'Албания',
                                'dz' => 'Алжир',
                                'vi' => 'Американские Виргинские острова',
                                'as' => 'Американское Самоа',
                                'ai' => 'Ангилья',
                                'ao' => 'Ангола',
                                'ad' => 'Андорра',
                                'aq' => 'Антарктида',
                                'ag' => 'Антигуа и Барбуда',
                                'ar' => 'Аргентина',
                                'am' => 'Армения',
                                'aw' => 'Аруба',
                                'af' => 'Афганистан',
                                'bs' => 'Багамы',
                                'bd' => 'Бангладеш',
                                'bb' => 'Барбадос',
                                'bh' => 'Бахрейн',
                                'bz' => 'Белиз',
                                'by' => 'Белоруссия',
                                'be' => 'Бельгия',
                                'bj' => 'Бенин',
                                'bm' => 'Бермуды',
                                'bg' => 'Болгария',
                                'bo' => 'Боливия',
                                'ba' => 'Босния и Герцеговина',
                                'bw' => 'Ботсвана',
                                'br' => 'Бразилия',
                                'io' => 'Британская территория в Индийском океане',
                                'vg' => 'Британские Виргинские острова',
                                'bn' => 'Бруней',
                                'bf' => 'Буркина Фасо',
                                'bi' => 'Бурунди',
                                'bt' => 'Бутан',
                                'vu' => 'Вануату',
                                'va' => 'Ватикан',
                                'uk' => 'Великобритания',
                                'hu' => 'Венгрия',
                                've' => 'Венесуэла',
                                'um' => 'Внешние малые острова (США)',
                                'tl' => 'Восточный Тимор',
                                'vn' => 'Вьетнам',
                                'ga' => 'Габон',
                                'ht' => 'Гаити',
                                'gy' => 'Гайана',
                                'gm' => 'Гамбия',
                                'gh' => 'Гана',
                                'gp' => 'Гваделупа',
                                'gt' => 'Гватемала',
                                'gn' => 'Гвинея',
                                'gw' => 'Гвинея-Бисау',
                                'de' => 'Германия',
                                'gi' => 'Гибралтар',
                                'hn' => 'Гондурас',
                                'hk' => 'Гонконг',
                                'gd' => 'Гренада',
                                'gl' => 'Гренландия',
                                'gr' => 'Греция',
                                'ge' => 'Грузия',
                                'gu' => 'Гуам',
                                'dk' => 'Дания',
                                'cd' => 'Демократическая Республика Конго',
                                'dj' => 'Джибути',
                                'dm' => 'Доминика',
                                'do' => 'Доминиканская Республика',
                                'eg' => 'Египет',
                                'zm' => 'Замбия',
                                'eh' => 'Западная Сахара',
                                'zw' => 'Зимбабве',
                                'il' => 'Израиль',
                                'in' => 'Индия',
                                'id' => 'Индонезия',
                                'jo' => 'Иордания',
                                'iq' => 'Ирак',
                                'ir' => 'Иран',
                                'ie' => 'Ирландия',
                                'is' => 'Исландия',
                                'es' => 'Испания',
                                'it' => 'Италия',
                                'ye' => 'Йемен',
                                'cv' => 'Кабо-Верде',
                                'kz' => 'Казахстан',
                                'ky' => 'Каймановы острова',
                                'kh' => 'Камбоджа',
                                'cm' => 'Камерун',
                                'ca' => 'Канада',
                                'qa' => 'Катар',
                                'ke' => 'Кения',
                                'cy' => 'Кипр',
                                'kg' => 'Киргизия',
                                'ki' => 'Кирибати',
                                'tw' => 'Китайская Республика',
                                'kp' => 'КНДР',
                                'cn' => 'КНР',
                                'cc' => 'Кокосовые острова',
                                'co' => 'Колумбия',
                                'km' => 'Коморы',
                                'cr' => 'Коста-Рика',
                                'ci' => 'Кот-д’Ивуар',
                                'cu' => 'Куба',
                                'kw' => 'Кувейт',
                                'la' => 'Лаос',
                                'lv' => 'Латвия',
                                'ls' => 'Лесото',
                                'lr' => 'Либерия',
                                'lb' => 'Ливан',
                                'ly' => 'Ливия',
                                'lt' => 'Литва',
                                'li' => 'Лихтенштейн',
                                'lu' => 'Люксембург',
                                'mu' => 'Маврикий',
                                'mr' => 'Мавритания',
                                'mg' => 'Мадагаскар',
                                'yt' => 'Майотта',
                                'mo' => 'Макао',
                                'mw' => 'Малави',
                                'my' => 'Малайзия',
                                'ml' => 'Мали',
                                'mv' => 'Мальдивы',
                                'mt' => 'Мальта',
                                'ma' => 'Марокко',
                                'mq' => 'Мартиника',
                                'mh' => 'Маршалловы Острова',
                                'mx' => 'Мексика',
                                'fm' => 'Микронезия',
                                'mz' => 'Мозамбик',
                                'md' => 'Молдавия',
                                'mc' => 'Монако',
                                'mn' => 'Монголия',
                                'ms' => 'Монтсеррат',
                                'mm' => 'Мьянма',
                                'na' => 'Намибия',
                                'nr' => 'Науру',
                                'np' => 'Непал',
                                'ne' => 'Нигер',
                                'ng' => 'Нигерия',
                                'an' => 'Нидерландские Антильские острова',
                                'nl' => 'Нидерланды',
                                'ni' => 'Никарагуа',
                                'nu' => 'Ниуэ',
                                'nz' => 'Новая Зеландия',
                                'nc' => 'Новая Каледония',
                                'no' => 'Норвегия',
                                'ae' => 'ОАЭ',
                                'om' => 'Оман',
                                'bv' => 'Остров Буве',
                                'nf' => 'Остров Норфолк',
                                'cx' => 'Остров Рождества',
                                'sh' => 'Остров Святой Елены',
                                'ck' => 'Острова Кука',
                                'pn' => 'Острова Питкэрн',
                                'pk' => 'Пакистан',
                                'pw' => 'Палау',
                                'ps' => 'Палестина',
                                'pa' => 'Панама',
                                'pg' => 'Папуа — Новая Гвинея',
                                'py' => 'Парагвай',
                                'pe' => 'Перу',
                                'pl' => 'Польша',
                                'pt' => 'Португалия',
                                'pr' => 'Пуэрто-Рико',
                                'cg' => 'Республика Конго',
                                'kr' => 'Республика Корея',
                                'mk' => 'Республика Македония',
                                're' => 'Реюньон',
                                'ru' => 'Россия',
                                'rw' => 'Руанда',
                                'ro' => 'Румыния',
                                'sv' => 'Сальвадор',
                                'ws' => 'Самоа',
                                'sm' => 'Сан-Марино',
                                'st' => 'Сан-Томе и Принсипи',
                                'sa' => 'Саудовская Аравия',
                                'sz' => 'Свазиленд',
                                'mp' => 'Северные Марианские острова',
                                'sc' => 'Сейшельские Острова',
                                'pm' => 'Сен-Пьер и Микелон',
                                'sn' => 'Сенегал',
                                'vc' => 'Сент-Винсент и Гренадины',
                                'kn' => 'Сент-Китс и Невис',
                                'lc' => 'Сент-Люсия',
                                'rs' => 'Сербия',
                                'cs' => 'Сербия и Черногория',
                                'sg' => 'Сингапур',
                                'sy' => 'Сирия',
                                'sk' => 'Словакия',
                                'si' => 'Словения',
                                'sb' => 'Соломоновы Острова',
                                'so' => 'Сомали',
                                'sd' => 'Судан',
                                'sr' => 'Суринам',
                                'us' => 'США',
                                'sl' => 'Сьерра-Леоне',
                                'tj' => 'Таджикистан',
                                'th' => 'Таиланд',
                                'tz' => 'Танзания',
                                'tg' => 'Того',
                                'tk' => 'Токелау',
                                'to' => 'Тонга',
                                'tt' => 'Тринидад и Тобаго',
                                'tv' => 'Тувалу',
                                'tn' => 'Тунис',
                                'tm' => 'Туркмения',
                                'tr' => 'Турция',
                                'tc' => 'Тёркс и Кайкос',
                                'ug' => 'Уганда',
                                'uz' => 'Узбекистан',
                                'ua' => 'Украина',
                                'wf' => 'Уоллис и Футуна',
                                'uy' => 'Уругвай',
                                'fo' => 'Фарерские острова',
                                'fj' => 'Фиджи',
                                'ph' => 'Филиппины',
                                'fi' => 'Финляндия',
                                'fk' => 'Фолклендские острова',
                                'fr' => 'Франция',
                                'gf' => 'Французская Гвиана',
                                'pf' => 'Французская Полинезия',
                                'tf' => 'Французские Южные и Антарктические Территории',
                                'hm' => 'Херд и Макдональд',
                                'hr' => 'Хорватия',
                                'cf' => 'ЦАР',
                                'td' => 'Чад',
                                'me' => 'Черногория',
                                'cz' => 'Чехия',
                                'cl' => 'Чили',
                                'ch' => 'Швейцария',
                                'se' => 'Швеция',
                                'sj' => 'Шпицберген и Ян-Майен',
                                'lk' => 'Шри-Ланка',
                                'ec' => 'Эквадор',
                                'gq' => 'Экваториальная Гвинея',
                                'er' => 'Эритрея',
                                'ee' => 'Эстония',
                                'et' => 'Эфиопия',
                                'za' => 'ЮАР',
                                'gs' => 'Южная Георгия и Южные Сандвичевы острова',
                                'jm' => 'Ямайка',
                                'jp' => 'Япония'
    );       
   
    /**
     * @param string $action
     * @return string 
     */    
    public function route($action){
        switch($action){
            case 'geo':    
                return 'geo';
            break;    
            case 'geo_renew':
                return 'geoRenew';
            break;
        }
        return false;
    }

    
    /**
     * Cities and countries 
     * @return array
     */
    public function geo(){
        $data = $this->handler->getCache('geo');                
        return $data ? $data : $this->geoRenew();
    }
    
    
    /**
     * Update cities and countries list
     * @return array
     */
    public function geoRenew(){
        $data = $this->getCountriesAndItsData();
        $this->handler->setCache('geo', $data);         
        return $data;
    }


    /**     
     * @access protected   
     * @return array
     */
    protected function getCountriesAndItsData(){
        set_time_limit(300);          
        $data = file_get_contents($this->data_url); 

        if($data) try{
            eval('$data = '.$data.';');
            $data['country'] = $this->countries;
            
            $urls = array();
            foreach($data['country'] as $code => $country) $urls[$code] = $this->country_data_url.$code;
            $cities_for_countries = FinderUtilities::getUrlsContent($urls);   

            $data['country_codes'] = array();
            $countries = array();

            if($cities_for_countries){ 
                foreach ($data['country'] as $code => $country){                    
                    if(!empty($cities_for_countries[$urls[$code]]['content'])){
                        $cities = $this->citiesAndAirports($cities_for_countries[$urls[$code]]['content']);
                    }
                    else $cities = array();           
                    
                    if(empty($cities['airports'])) continue;
                    $countries[] = array_merge(
                            array(
                                'key'=>$code, 
                                'name'=>$country
                            ), 
                            $cities);
                    $data['country_codes'][$code] = count($countries)-1;
                }
                $data['country'] = $countries;
            }
            
            if(isset($data['city'])){
                $cities = array();                
                foreach ($data['city'] as $k => $v){
                    $cities[] = array('key' => $k, 'name' => $v);
                    $data['city_codes'][$k] = count($cities)-1;
                }
                $data['city'] = $cities;
            }              
        }catch(\Exception $e){
            return false;
        }
        return $data;    
    }
    
    
    /**     
     * @param array $data
     * @return array
     */
    protected function citiesAndAirports($data){            
        $data = FinderUtilities::getObjectFromJsFunctionCall($data);        
        $result = array(
            'cities' => isset($data['town']) ? (array) $data['town'] : array(), 
            'airports' => isset($data['airport']) ? (array) $data['airport'] : array()
        );
        sort($result['cities']);
        sort($result['airports']);
        return $result;
    }
    
    
    
}