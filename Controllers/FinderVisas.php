<?php
/**
 * Visas handling
 * @author redlobster
 */


namespace Finder;


class FinderVisas extends FinderRealmHandler{  
    
    
    public function route($action) {
        
        switch($action){
            case 'visasgeo':
                return 'getVisasCitiesCountries';
            break;
            case 'visatext':   
               return 'visaText';
            break;
        }
        
        return false;        
    }

    
    /**
     * Visa conditions for country, city
     * @return array
     */
    public function visaText(){           
        $sql = "      
            SELECT visa.text, visa.url        
            FROM visa        
            INNER JOIN country ON country.id = visa.country_id        
            INNER JOIN city ON city.id = visa.city_id       
            WHERE country.code='".pg_escape_string($this->params['country'])."' AND city.code='".pg_escape_string($this->params['city'])."'";
        $data = $this->handler->getRtList($sql, false);                    
        return $data ? $data[0] : false;
    }
    
    
    /**
     * List of countries with known visa conditions and cities, where visa can be derived.
     * @return array
     */
    public function getVisasCitiesCountries(){
        $sql = ' SELECT DISTINCT country.name AS country_name,                     
                                 country.code AS country_code,                     
                                 city.name AS city_name,
                                 city.code AS city_code
                 FROM visa 
                 INNER JOIN country ON country.id = visa.country_id 
                 INNER JOIN city ON city.id = visa.city_id 
                 ORDER BY country.name ';
        $data = $this->handler->getRtList($sql, false);

        $cities_by_countries = [];
        foreach($data as $row){
            if(empty($cities_by_countries[$row['country_code']])) $cities_by_countries[$row['country_code']] = [
                'key' => $row['country_code'], 
                'name' => $row['country_name'], 
                'cities' => []
            ];
            $cities_by_countries[$row['country_code']]['cities'][] = ['name' => $row['city_name'], 'key' => $row['city_code']];
        }
        return array_values($cities_by_countries);        
    }

    
}
