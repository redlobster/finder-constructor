<?php
/**
 * Set of useful simple functions
 *
 * @author redlobster
 */

namespace Finder;

include_once 'memcache.php';


class FinderUtilities {   
    
    protected static $instance = null;
            
    /**
     * Prefix to divide cache data between versions
     * @access protected
     * @var string 
     */
    protected $cache_prefix;
    
    /**
     * Cache handler
     * @acess protected
     * @var MemcacheHandler
     */
    protected $cache;
    
    /**
     * Time of cache update
     * @access protected
     * @var string|bool 
     */
    protected $modified = false;
    
    /**
     * Timeout setting for curl queries
     * @access protected
     * @var int
     */
    protected static $timeout = 95;
    
    /**
     * PGSQL connection resource
     * @access protected 
     * @var resource
     */
    protected $rtdb;    
    
    /**
     * Data for connection to base
     * @access protected
     * @var array
     */
    protected $rtdb_params = array(
        'host' => '82.151.193.201', 
        'port' => '5432',
        'user' => 'postgres', 
        'password' => '1qazxsw2',
        'dbname' => 'postgres'
    );

    protected function __construct(){
        $this->cache_prefix = "tour_finder_".__DIR__;
    }

    
    protected function __clone(){}

    /**
     * Some kind of singleton
     * @return FinderUtilities
     */
    public static function getInstance(){
        if (!isset(static::$instance)) static::$instance = new static;
        
        return static::$instance;
    }
    
    
    /**
     * Get memcache handler
     * @access protected
     * @return MemcacheHandler
     */
    protected function getCacheHandler(){
        if(empty($this->cache)) $this->cache = new MemcacheHandler();    
        return $this->cache;
    }


    /**
     * Get from cache by key
     * @param string $key
     * @return array
     */
    public function getCache($key){
        $data = $this->getCacheHandler()->get($this->cache_prefix.$key);
        if($data) $this->modified = $this->getCacheHandler()->get($this->cache_prefix.$key.'_modified');
        return $data;
    }
    
    
    /**
     * Get time of cache data update
     * @return int
     */
    public function getCacheModified(){
        return $this->modified;
    }
    
    
    /**
     * Set data for key in cache and time in suitable for header format
     * @param string $key
     * @param string $data
     * @param int $time
     */
    public function setCache($key, $data, $time = 2592000){               
       $this->getCacheHandler()->set($this->cache_prefix.$key, $data, $time);
       $this->getCacheHandler()->set($this->cache_prefix.$key.'_modified', self::getDateFormatted(), $time);              
    }

    
    /**
     * Send mail by smtp
     * @access static
     * @param string $message
     * @param array $emails
     * @return bool
     */
    static function sendMail($message, array $emails){
        $source_path = '/var/www/lib/';
        if(!is_file($source_path.'core.php')){
            $connection = ssh2_connect('finder.ross-tur.ru', 22);
            ssh2_auth_password($connection, 'ross', 'Folk97Hot');
            $sftp = ssh2_sftp($connection);
            $source_path = "ssh2.sftp://$sftp".$source_path;
        }

        include_once $source_path."cls.php";
        include_once $source_path."fn.php";
        include_once $source_path."core.php";
        include_once $source_path."phpMailer/class.phpmailer.php";
        include_once $source_path."smtpmail.php";

        $mail =  \_::SmtpMail();
        $mail->options['address'] = $emails;
        $mail->options['FromEmail'] = 'it@ross-tur.ru';
        $mail->options['FromName'] = 'Rosstour';
        $mail->options['message'] = $message;
        $mail->options['subject'] = 'Заявка на отель+авиаперелет с сайта Росс-Тур';
        return $mail->send();   
    }
    
    
    /**     
     * @param int $time
     * @return string
     */
    static function getDateFormatted($time=null){
        if(!$time) $time = time();
        return gmdate("D, d M Y H:i:s",$time)." GMT";
    }
    
    
    
    static function paramsToUrl($url, $params, $prefix=''){
        foreach($params as $k => $v){
            if(is_array($v)) $url = self::paramsToUrl($url, $v, $prefix ? $prefix.'['.$k.']':$k);
            else $url .= ($prefix ? $prefix.'['.$k.']' : $k).'='.urlencode($v).'&';
        }
        return $url;
    }
    
    
    static function getDataByCurl($url, $referer=null){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);               
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, self::$timeout); 
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::$timeout); 
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        if($referer) curl_setopt($ch, CURLOPT_REFERER, $referer);
        $response = curl_exec($ch);
        curl_close($ch); 
        return $response;
    }
    
    
    static function getUrlsContent($urls) {
        $mh = curl_multi_init();        

        $chs = array();
        foreach ( $urls as $url ) {
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_HEADER, 0 );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt( $ch, CURLOPT_TIMEOUT, 60);
            curl_multi_add_handle( $mh, $ch );
            $chs[$url] = $ch;
        }
        
        $results = array();         
        
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        }
        while ($running > 0);

        // Get content and remove handles.
        foreach ($chs as $key => $val) {
            $results[$key] = array('content' => curl_multi_getcontent($val));
            curl_multi_remove_handle($mh, $val);
        }

        curl_multi_close($mh);
 
        // результаты
        return $results;
    } 
    
    
    static function getObjectFromJsFunctionCall($data){
        $data = preg_replace('/.*?\((\{.*\})\);.*/s', '$1', $data);
        return (array) json_decode( $data , true);     
    } 
    
    
    static function handleRequest($url, $params){
        $url = self::paramsToUrl($url, $params);  
        $data = self::getDataByCurl($url);  
        $data = json_decode( $data, true );
        return $data;
    }
    
    
    static function handleScriptRequest($url, $params){
        $url = self::paramsToUrl($url, $params); 
        $data = self::getDataByCurl($url);
        $data = self::getObjectFromJsFunctionCall($data);
        return $data;
    }    
    
    
    static function getFromApi($service, $method, array $data = array()){
        $url = 'http://api.ross-tur.ru/gate/?';
        
        $params = array(
            'service' => $service,
            'method' => $method,
            'format' => 'json',
            'apiKey' => 'directaccess',            
        );
        
        $params = array_merge($params, $data);
        return self::handleRequest($url, $params);        
    }
    
    
    protected function getRtDb(){
        if(empty($this->rtdb)){
            $this->rtdb = pg_connect("host=".$this->rtdb_params['host']." dbname=".$this->rtdb_params['dbname']." user=".$this->rtdb_params['user']." password=".$this->rtdb_params['password']) or die('Could not connect: ' . pg_last_error());
        }
        return $this->rtdb; 
    }
    
    
    protected function makeRtQuery($sql){
        $db = $this->getRtDb();
        return pg_query($db, $sql);	
    }
    
    
    public function getRtValue($sql){
        $resource = $this->makeRtQuery($sql);
        if(!$resource) return false;
        $data = pg_fetch_assoc($resource);
        return $data ? current($data) : false;
    }
    
    
    public function getRtList($sql, $by_key = 'id'){
        $resource = $this->makeRtQuery($sql);
        $result = array();
        if ($resource)   while($row = pg_fetch_array($resource, null, PGSQL_ASSOC)){
            if($by_key && !empty($row[$by_key])) $result[$row[$by_key]] = $row; 
            else $result[] = $row; 
        }
        return $result ? $result : false;    
    }
    
    
}

?>
