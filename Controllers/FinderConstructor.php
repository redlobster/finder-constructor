<?php

/**
 * Hotel + Flight Controller
 * @author redlobster
 */

namespace Finder;

class FinderConstructor extends FinderRealmHandler {        
    
        protected $markup = 0.2;   
        
        public function route($action){
            switch($action){
                case 'flights':
                    return 'getFlights';
                break;
                case 'offices':
                    return 'getOffices';              
                break;        
                case 'cancellation':
                    return 'getCancellationData';
                break;
                case 'visa':
                    return 'getVisaData';
                break;
                case 'hotels':                
                    return 'getHotels';           
                break;
                case 'filter':
                    return 'filterHotels';
                break;
                case 'passport':
                    return 'checkPassport';
                break;
                case 'reserve_flight':
                    return 'reserveFlight';           
                break;
                case 'reserve_hotel':
                    return 'reserveHotel';                  
                break;      
                case 'rates':
                    return 'getRates';
                break;    
                case 'mail':
                    return 'sendMail';
                break;  
            }
            return false;
        }
        
        /**
         * Visa data, used for getting country id in check passport action
         * @return array
         */
        function getVisaData(){
            $url = 'http://rt.ross-tur.ru/hotel/5/index.php?a=init&oper=ALL';
            return FinderUtilities::handleScriptRequest($url, array());      
        }        
        
        
        /**
         * Flights list for search params
         * @return array
         */        
        function getFlights(){ 
            list($adults, $children) = $this->handleAdultsChildrenRequest();
            
            $this->init();
            $sid = empty($_SESSION['init_sid'])?'':$_SESSION['init_sid'];        

            $params = array(
                'a' => 'find',            
                'agency' => '0',
                'aviatype' => '0',                       
                'filter_carrier' => '',	
                'filter_day' => '',	
                'filter_marga' => '',	
                'filter_oper' => '',	
                'infants' => '0',
                'markup' => $this->markup*100,
                'noconnect' => (empty($this->params['noconnect']) || $this->params['noconnect']=='0' ? '1' : 0 ),
                'page' => '0',            
                'sessionid' => $sid,
                'sort' => 'sum',
                'adult' => $adults,
                'child' => $children,
                'class' => (empty($this->params['class']) ? '0' : $this->params['class']),
                'seachtype' => (!empty($this->params['best_price'])&&$this->params['best_price']==1 ? 'NEARESTDATES3' : 'FLIGHTS'),
                'day' => array($this->params['date_from'], $this->params['date_to']),
                'city'=> array($this->params['city'], $this->params['airport']),
                'time' => array($this->params['time_from'], $this->params['time_to']),
                'version' => '6'
            );

            $data = FinderUtilities::getFromApi('avia', 'search', $params);      
            $data = $data && is_array($data) ? array_merge($data['data'], array('sid' => $sid)) : array('find' => array());        
            return $data;
        }
                
        
        /**
         * Getting total quantity of adults and children for all rooms
         * @return array
         */
        protected function handleAdultsChildrenRequest(){
            $children = 0;
            if(!empty($this->params['children'])){
                $this->params['children'] = array_map ('json_decode', $this->params['children']);
                foreach($this->params['children'] as $c) $children += count($c);
            }
            $this->params['adults'] = array_map('intval', $this->params['adults']);
            $adults = array_sum($this->params['adults']);
            return array($adults, $children);
        }
        
        
        /**
         * Start session to use its id in other api queries         
         */
        protected function init(){
            $params = array( 
                'from' => $this->params['city'], 
                'to' => $this->params['airport'],
                'version'=>6 
            );
            $data = FinderUtilities::getFromApi('avia', 'init', $params);
            if(!empty($data['data']['sessionid']))$_SESSION['init_sid'] = $data['data']['sessionid'];          
        }
        
        
        
        /**
         * Offices list for choosing email destination
         * @return array
         */
        function getOffices(){
            if(!empty($this->params['city'])) $city = $this->params['city'];
            else $city = '';

            return FinderUtilities::getDataByCurl('http://finder.ross-tur.ru/main/0.0.0.1/ajax/finder.php?a=getOffices'.($city ? '&city='.$city:''), 'http://finder.ross-tur.ru');
                              
        }  
        
        
        /**
         * Hotel booking cancellation conditions
         * @return array 
         */
        function getCancellationData(){
            $data = json_decode($this->params['data'], true);

            $data = array(
                'a' => 'cancelation',
                'version' => 1,            
                'currency' => $data['currency'],
                'hotel_ind' => $data['hotel_ind'],
                'oper' => $data['oper'],
                'room_ind' => $data['room_ind'],
                'sessionid' => $data['sessionid']
            );
            $result =  FinderUtilities::getFromApi('hotels', 'CancellationInfo', $data);      
            return empty($result['data']) ? [] : $result['data'];
        }
        
        
        /**
         * Hotels list for search params
         * @return array
         */        
        function getHotels(){
            $this->handleAdultsChildrenRequest();

            $params = array(
                'city' => $this->params['living'],
                'country' => $this->params['country'],
                'currency' => $this->params['currency'],
                'nights' => (int)$this->params['duration'],
                'start' => $this->params['date_from'],
                'statusRequest' => 'true',
                'stop' => $this->params['date_to'],
                'roper'=> 'work'
            );

            foreach ($this->params['adults'] as $k => $v){
                $params['rooms'][(string) $k] = array(
                    'children' => $this->params['children'][$k],
                    'quantity' => 1,
                    'adult' => $v
                );
            }                       

            $result = FinderUtilities::getFromApi('hotels', 'searchHotels', $params);  

            if($result){
                if(!empty($result['data']['services']['forfilter'])){                   
                    $result['data']['services']['forfilter'] = $this->handleFilters($result['data']['services']['forfilter']);
                }
                if(!empty($result['data']['find'])){
                    foreach($result['data']['find'] as $i => $item){
                        foreach ($item['room'] as $i2 => $room){
                            $result['data']['find'][$i]['room'][$i2]['price'] = ceil($room['pricenetto']*(1+$this->markup));
                            unset($result['data']['find'][$i]['room'][$i2]['priceinothercurrency']);
                            unset($result['data']['find'][$i]['room'][$i2]['pricenettoRUR']);
                        }
                    }
                }
            }

            return empty($result['data']) ? [] : $result['data'];
        } 
        
        
        /**
         * Conversion filters data to js convinient form
         * @param array $filters
         * @return array
         */
        protected function handleFilters($filters){
            foreach($filters as $filter_name => $filter){                
                if(!in_array($filter_name, array('star','food'))) continue;

                $data = array();
                foreach($filter as $k => $v){
                    $data[] = array('key' => $v[0], 'label' => $v[1], 'name' => $v[2]);
                }
                $filters[$filter_name] = $data;
            }
            return $filters;
        }
        
        
        /**
         * Left hotel list filter panel handler
         * @return array
         */
        function filterHotels(){
            $params = [
                'fhname' => empty($this->params['query']) ? '' : addslashes($this->params['query']),
                'sessionid' => $this->params['sid'],
                'sortorder' => 'asc',
                'tmpMeal' => !empty($this->params['meal']) && is_array($this->params['meal']) ? implode('-', array_filter($this->params['meal'])).'-':'1-2-3-4-5-NN',
                'tmpStar' => !empty($this->params['star']) && is_array($this->params['star']) ? implode('-', array_filter($this->params['star'])).'-':'BB-HB-FB-RO-NN'
            ];

            $result = FinderUtilities::getFromApi('hotels', 'filterHotels', $params);  
            if($result && !empty($result['data']['services']['forfilter'])){           
                $result['data']['services']['forfilter'] = $this->handleFilters($result['data']['services']['forfilter']);
            }
            return $result;
        }
        
        
        /**
         * Checking passport expire date for certain country
         * @return array
         */
        function checkPassport(){
            $params = array(
               'action' => 'check_passport',
               'callback' => 'some_func',
               'country_id' => $this->params['country'],
               'reis1' => $this->params['arrival'],
               'reis2' => $this->params['departure'],
               'zagran_date' => $this->params['passport']
            );

            $url = 'http://online-rosstour.ru/external/hotel/ext_persona_rt.php?';        
            $url = FinderUtilities::paramsToUrl($url, $params);        
            $data = FinderUtilities::getDataByCurl($url);   
            return ['status' => (strpos($data, 'status:1') > 0 ? 1 : 0)];
        }
        
        
        /**
         * Flight booking
         * @return array
         */
        function reserveFlight(){
            $order = (array) json_decode($this->params['order'], true);

            $params = array(
                'bonuscard' => empty($order['bonuscard']) ? '' : $order['bonuscard'],	            
                'comment' => empty($order['comment']) ? '': $order['comment'],
                'email' => empty($order['email'])?'avia3@rosstour.ru':$order['email'], 
                'findselect' => $order['select'],
                'phone' => empty($order['phone'])?'88001009930':$order['phone'],
                'sessionid' => $order['sid'],
                'HotelAndAviaSum' => $order['HotelAndAviaSum']
            );

            foreach($order['people'] as $i => $person){
                $person = (array) $person;
                $params['get'][$i] = array();
                $params['get'][$i][] = strtoupper($person['lastname']);
                $params['get'][$i][] = strtoupper($person['firstname']);                        
                $params['get'][$i][] = $person['birth'];
                $params['get'][$i][] = $person['gender'];
                $params['get'][$i][] = 'FOREIGN';
                $params['get'][$i][] = preg_replace('/\D/', '', $person['passport']);
                $params['get'][$i][] = $person['passport_date'];
                $params['get'][$i][] = 'RU';
            }           

            $data = array_merge($params, array(
                'a' => 'reservation',
                'version' => 6,
                'col' => count($params['get'])
            ));
            
            $data = FinderUtilities::getFromApi('avia', 'reservation', $data);  
            return empty($data['data']) ? [] : $data['data'];
        }
        
        
        
        /**
         * Hotel booking
         * @return array
         */
        function reserveHotel(){
            $order = (array) json_decode($this->params['order'], true);        

            $data = array(
                'LastDayWithoutPenality' => $order['rt-LastDayWithoutPenality'],
                'city' => $order['rt-to-city-id'],
                'country' => $order['rt-to-country-id'],
                'nights' => $order['rt-night'],
                'rt-born-pasport-deistvitelendo' => $order['rt-born-pasport-deistvitelendo'],
                'rt-born-pasport-number' => $order['rt-born-pasport-number'],
                'rt-born-pasport-serial' => $order['rt-born-pasport-serial'],
                'rt-born-visa-consulstvo' => $order['rt-born-visa-consulstvo'],
                'rt-tourist-born' => $order['rt-tourist-born'],
                'rt-tourist-fi-eng' => $order['rt-tourist-fi-eng'],
                'rt-tourist-i-eng' => $order['rt-tourist-i-eng'],
                'rt-tourist-f-eng' => $order['rt-tourist-f-eng'],
                'rt-sex' => $order['rt-sex'],
                'rt-oper-alias' => $order['rt-oper-alias'],
                'rt-oper-id' => $order['rt-oper-id'],
                'sid' => $order['rt-session-id'],
                'start' =>$order['rt-from-date'],
                'stop' => $order['rt-to-date'],
                'rt-roomcode' => $order['rt-roomcode'],
                'rt-roomoptionid' => $order['rt-roomoptionid'],
                'rt-rooms' => array_sum(explode(',', $order['rt-rooms-count'])),
                'rt-session-hotel-code' => $order['hotelcode'],
                'rt-room' => implode(',',$order['rt-roomcode']),
                'rt-meal' => $order['rt-meal'],
                'rt-cost-visa' => $order['visa_cost'],
                'rt-cost-med' => $order['rt-personal-med'],
                'orderid' => $order['orderid'],
                'rt-valute'=> $order['rt-valute']
            );

            $data['rt-cost-med'] = array();
            foreach ($order['rt-tourist-fi-eng'] as $k => $v ) {
                $data['rt-cost-med'][$k] = (!empty($order['rt-personal-med'][$k]) ? 'on' : 'off');
            }

            foreach ($order['children'] as $k => $v){
                $data['rooms'][$k] = array('adult' => $order['adults'][$k], 'quantity'=>1);                    
                if($v) $data['rooms'][$k]['children'] = $v;            
            };

            //и про визу
            $data['people'] = array();
            foreach($order['rt-born-visa-consulstvo'] as $k => $v){
                $data['people'][$k]['visa'] = ((int) $v ? 'on' : $v);
            }

            $data = array_merge($data, array(
                'rt-tourist-passNum' => '1',
            ));

            $data = FinderUtilities::getFromApi('hotels', 'Brone', $data);  
            return empty($data['data']) ? [] : $data['data'];
        }
        
        
        /**
         * Our rates to display it on form
         * @return array
         */
        function getRates(){
            $data = $this->handler->getRtList("SELECT usd, eur, gbp FROM oper WHERE alias='own'");
            $data = $data[0];       
            return $data;
        }                                  
        
    
        /**
         * Send application email from user to office
         * @return bool 
         */
        function sendMail(){
            ini_set('display_errors', 0);
            error_reporting(0);
            $order = json_decode($this->params['order'], true);
            $message = $this->createMessage($order);                        

            $sender = array($order['email'], 'Rosstour');
            $office = array($order['office']['EMAIL'], 'Rosstour');
            $vadim = array('error@rosstour.ru', 'Rosstour');

            return FinderUtilities::sendMail($message, array($office, $sender, $vadim));        
        }    
    
   
        
        /**
         * Make email message from data array
         * @access protected
         * @param array $data
         * @return string
         */
        protected function createMessage($data){
            $rooms = implode(',',$data['rooms']);
            $message = <<<MESSAGE
                <div style="border-radius:5px;border:1px solid #d81910;background:#d81910;color:#fff;width:300px;">
                    Турист: {$data['sender']}<br>
                    Телефон: {$data['phone']}<br>
                    Email: {$data['email']}<br>
                </div>
                <p>
                    <b>Офис, куда отправлена заявка:</b> {$data['office']['NAME']}; г. {$data['office_city']}, {$data['office']['ADDRESS']};  {$data['office']['PHONE']}
                </p>    
                <p>    
                    <b>Город отправления:</b> {$data['city']}
                </p>    
                <p>    
                    <b>Страна прибытия:</b> {$data['country']}
                </p>  
                <p>    
                    <b>Аэропорт:</b> {$data['airport']}
                </p>      
                <p>    
                    <b>Город проживания:</b> {$data['living']}
                </p>    
                <p>    
                    <b>Вылет:</b> {$data['departure']}
                </p>    
                <p>    
                    <b>Прибытие:</b> {$data['arrival']}
                </p>  
                <p>    
                    <b>Продолжительность:</b> {$data['duration']}
                </p>      
                <p>    
                    <b>Система бронирования гостиницы:</b> {$data['touroper']}
                </p>    
                <p>    
                    <b>Авиаперевозчик:</b> {$data['aviaoper']}
                </p>  
                <p>    
                    <b>Гостиница:</b> {$data['hotel']}
                </p>    
                <p>    
                    <b>Номера:</b> {$rooms} 
                </p>
                <p>    
                    <b>Питание:</b> {$data['meal']} 
                </p>  
                <p>    
                    <b>Валюта гостиницы:</b> {$data['currency']}
                </p>     
                <p>    
                    <b>Сумма в валюте гостиницы:</b> {$data['sum']}
                </p>    
                <p>    
                    <b>Сумма в рублях:</b> {$data['sum_rur']}
                </p>   
                <p>    
                    <b>Курс USD:</b> {$data['rates']['USD']}
                </p>    
                <p>    
                    <b>Курс EUR:</b> {$data['rates']['EUR']}
                </p>       
                <table style="border:0px solid #999;border-radius:5px;">
                    <tbody>
                        <tr style="background:#999;color:#fff;">
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Пол</th>
                            <th>Дата рождения</th>
                            <th>Паспорт</th>
                            <th>Дата выдачи</th>
                            <th>Страховка</th>
                            <th>Виза</th>
                            <th>Консульство</th>
                        </tr>
MESSAGE;
        
            if(!empty($data['comment'])){
                $message .= " <p>    
                    <b>Комментарий:</b> {$data['comment']}
                </p>";
            }   
            if(!empty($data['bonuscard'])){
                $message .= " <p>    
                    <b>Карта РоссТур:</b> {$data['bonuscard']}
                </p>";
            }   
            foreach($data['people'] as $person):   
                $gender = $person['gender']=='w'?'женский':'мужской';
                $insurance = ((int)$person['insurance']) ? 'нужна':'не нужна';
                $visa = ((int)$person['visa']) ? 'нужна':'не нужна';
                $visacity = ($person['visacity'] && (int)$person['visa']) ? $person['visacity']:'';
                $message .= <<<MESSAGE
                        <tr style="background:#f1f1f1;color:#000;text-transform:uppercase;">
                            <td style="padding:5px 2px;">{$person['lastname']}</td>
                            <td style="padding:5px 2px;">{$person['firstname']}</td>
                            <td style="padding:5px 2px;">{$gender}</td>
                            <td style="padding:5px 2px;">{$person['birth']}</td>
                            <td style="padding:5px 2px;">{$person['passport']}</td>
                            <td style="padding:5px 2px;">{$person['passport_date']}</td>
                            <td style="padding:5px 2px;">{$insurance}</td>
                            <td style="padding:5px 2px;">{$visa}</td>
                            <td style="padding:5px 2px;">{$visacity}</td>
                        </tr>		 
MESSAGE;

            endforeach;
            $message .= '</tbody>
                </table>   ';        
            return $message;
        }
           
}
