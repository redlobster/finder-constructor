<?php
/**
 * Controller for getting js, css, templates data from cache
 *
 * @author redlobster
 */


namespace Finder;

class FinderSources extends FinderRealmHandler{
    
    /**
     * @access protected
     * @var string 
     */
    protected $root;
    
    /**
     * @access protected
     * @var array
     */
    protected $js_order = array('libs', 'plugins', 'directives', 'filters', 'finder_form_controller.js');    
    

    public function route($action) {
        
        switch($action){
            case 'clear':
                return 'renewAll';
            break;
            case 'js':
                return 'js';
            break;
            case 'css':
                return 'css';
            break;
            case 'templates':
                return 'templates';
            break;
        }
        return false;
    }
    
    
    
    function __construct(array $params = null) {
        parent::__construct($params);
        $this->root = realpath(__DIR__.'/../');
    }
    
    
    /**     
     * @return string
     */
    public function js(){
        $data = $this->handler->getCache('js');
        return $data ? $data : $this->renewSource('js');           
    }
    
    /**    
     * @return string
     */
    public function css(){
        $this->content_type = 'text/css';    
        $data = $this->handler->getCache('css');
        return $data ? $data : $this->renewSource('css');           
    }
    
    /**
     * Return array of html templates from html folder
     * @return array
     */
    public function templates(){
        $data = $this->handler->getCache('html');
        return $data ? $data : $this->renewSource('html');        
    }
    
    
    /**
     * Update js, css and template cache
     * @return string
     */
    public function renewAll(){
        if( $this->renewSource('css') && 
            $this->renewSource('html') && 
            $this->renewSource('js')
        ) return 'Js, css and templates successfully refreshed.';
        return 'Unable to refresh sources.';
    }
    
    
    /**
     * Update certain source in cache
     * @param string $type js|css|html
     * @return string|array
     */
    protected function renewSource($type){
        //get files
        $method = "get".ucfirst($type);
        if(!method_exists($this,$method)) return false;
        $files = $this->$method();
        //handle files
        $method = "handle".ucfirst($type);
        if(method_exists($this,$method)) $data = $this->$method($files);
        else $data = $this->handle($files);
        //cache data
        $this->handler->setCache($type, $data);
        return $data;
    }
    
    /**
     * List of js files pathes
     * @access protected
     * @return array
     */    
    protected function getJs(){
        $dir = "$this->root/js/form";
        return $this->getFilesList($dir, 'js');        
    }

    
    /**     
     * @access protected
     * @return array
     */
    protected function getCss(){
        $dir = "$this->root/css";
        return $this->getFilesList($dir, 'css');
    }
    
    
    /**
     * 
     * @access protected
     * @return array
     */
    protected function getHtml(){
        $dir = "$this->root/html";
        return $this->getFilesList($dir, 'html');
    }
    
    /**
     * @access protected
     * @param string $dir
     * @param strind $type
     * @return array
     */
    protected function getFilesList($dir, $type){
        $files = array();
        $sources = scandir($dir);
        if(
            $type == 'js' && 
            count(array_intersect($sources, $this->js_order)) == count($this->js_order)
        ){//вначале сторонние библиотеки и директивы
            $sources = array_merge($this->js_order, array_diff($sources, $this->js_order));
        }
        foreach($sources as $file){
            $path = "$dir/$file";
            if(is_dir($path) && trim($file,'.')) $files = array_merge ($files, $this->getFilesList ($path, $type));
            elseif(is_file($path) && pathinfo($path, PATHINFO_EXTENSION) == $type){
                $files[] = $path;
            }
        }
        return $files;
    }
    
    
    /**
     * Paste together files contents
     * @access protected
     * @param array $files
     * @return string
     */
    protected function handle(array $files){
        return $this->getFilesContent($files);     
    }
    
    
    /**     
     * @param array $files
     * @return array
     */
    protected function handleHtml(array $files){
        $data = array();
        foreach ($files as $file){
            $name = basename($file, ".html");
            $data[$name] = file_get_contents($file);
        } 
        return $data;
    }

    /**
     * @access protected
     * @param array $files
     * @return string
     */    
    protected function getFilesContent($files){
        $data = '';
        foreach ($files as $file) $data .= file_get_contents($file).PHP_EOL.PHP_EOL; 
        return $data;
    }
}