<?php
/**
 * Parent class for realm controllers
 * 
 * @author redlobster
 */

namespace Finder;

abstract class FinderRealmHandler {
    
    /**    
    * @var array
    * @access protected
    */
    protected $params;
    
   /**    
    * @var FinderUtilities
    * @access protected
    */
    protected $handler;
    
    
    /**    
    * @var string
    * @access public
    */
    public $content_type = 'text/javascript';       
    
    
    public function __construct(array $params = null) {
        $this->params = $params;   
        $this->handler = FinderUtilities::getInstance();
    }  
    
    /**
     * Search handler for given action
     *
     * @param string $action     
     * @return string
     */
    abstract function route($action);    
}
