<?php
/**
 * This is core. Enter point. Main Controller.
 *
 * @author redlobster
 */

namespace Finder;

class FinderCore {       
    /**    
    * @var array
    * @access protected
    */
    protected $output;    
       
    /**    
    * @var array
    * @access protected
    */
    protected $params;
    
   /**    
    * @var FinderUtilities
    * @access protected
    */
    protected $handler;
    
    
    /**    
    * @var FinderUtilities
    * @access protected
    */
    protected $controller;
    
    
    /**
     * This is constructor. Routing start here.
     * @param array $params GET params
     */
    function __construct($params) { 
        if(empty($params['action'])) throw new \Exception('No action specified');      
        $this->handler = FinderUtilities::getInstance();
        $this->params = $params;   
        $this->route();                  
    }   
           
        
    /**
     * Here we determine actual controller and its action, calling method route for every realm controller until any of it will recognize the given action param.
     * @access protected
     * @throws \Exception
     */
    protected function route(){    
        $use_cached = !empty($this->params['clear']);
                        
        $controllers = scandir(__DIR__);       
        foreach ($controllers as $file){
            $class = __NAMESPACE__."\\".basename($file,'.php');              
            if(
                    class_exists($class) && 
                    get_parent_class($class) == __NAMESPACE__.'\FinderRealmHandler'
            ){
                $controller = new $class($this->params);
                $action = $controller->route($this->params['action']);                
                if($action){
                    $this->controller = $controller;
                    $data = $this->controller->$action();
                    $this->output = is_string($data) ? $data : ['success' => !empty($data), 'data' => $data];                   
                }                
            }            
        }
        if(empty($this->controller)) throw new \Exception('action is invalid');
    }
    
    
    /**
     * Sending headers with content type and time of its update
     * @return boolean
     */
    protected function headers(){        
        header("Content-Type: ".$this->controller->content_type);  
        $modified = $this->handler->getCacheModified();
        if($modified){
            header("Expires: ");          
            header('Last-Modified: '.$modified);
            header('Cache-control: no-cache, must-revalidate');
            if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE']==$modified){
                header ("HTTP/1.1 304 Not Modified ");
                die();
            }
        }
        return true;
    }
    
    
    /**
     * Output content suitable for all, including json, queries     
     */
    function output(){
        if($this->headers()){
            $callback = empty($this->params['callback']) ? false : addslashes($this->params['callback']);
            
            if(!is_string($this->output) || $callback) $this->output = json_encode($this->output);                    
            $this->output = $callback ? "$callback($this->output)": $this->output;
            
            echo $this->output;
        }
    }
    
}

