<?php

require_once __DIR__.'/vendor/src/Symfony/Component/ClassLoader/ClassMapGenerator.php';
require_once __DIR__.'/vendor/src/Symfony/Component/ClassLoader/MapClassLoader.php';

use Symfony\Component\ClassLoader\ClassMapGenerator;
use Symfony\Component\ClassLoader\MapClassLoader;

$map = ClassMapGenerator::createMap(__DIR__);

$loader = new MapClassLoader($map);
$loader->register();
