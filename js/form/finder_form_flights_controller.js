//контроллер списка перелетов
tourFinderApp.controller('TourFinderFlightsCtrl', function($scope, $rootScope, $http, $filter, $timeout, Rates) {
    
    var sortFlights = function(){
        $scope.flightsBySimilar = {};
        var i;
        for(i=0; i < $rootScope.flights.find.length; i++){
            if(typeof $scope.flightsBySimilar[$rootScope.flights.find[i].similar] == 'undefined'){
                $scope.flightsBySimilar[$rootScope.flights.find[i].similar] = [];
            }
            $rootScope.flights.find[i].list_index = i;
            $scope.flightsBySimilar[$rootScope.flights.find[i].similar].push( $rootScope.flights.find[i] );
        } 
    }
    
    
    $scope.getSimilarKeys = function(){
        return Object.keys($scope.flightsBySimilar);
    }
    
    
    $rootScope.$watch('flights', function(newValue, oldValue) {        
        sortFlights();
        $scope.toPage(0);
    });
    
    
    $rootScope.$watch('view', function(n,o){
        if(n == 'flights'){
            $timeout(function(){
                $(window).scrollTop($('.selected_flight').eq(0).offset().top);        
            }, 400);
        }
    });
            
    
    $scope.calculateFlightDifference = function(i){
        var difference = parseFloat( $rootScope.flights.find[i].sum ) - parseFloat($rootScope.flights.min.sumMin);        
        return Rates.convert(difference,'RUR','EUR',false);
    }
    
       
    $scope.getFlightsIndexForPage = function(i){
        return $rootScope.getIndexForPage(i, $scope.page);
    }
    
    
    $scope.toPage = function(page){
        $scope.page = page;
        $scope.pages_list = $scope.getFlightsPages();
        $scope.difference=[];
        $scope.similar_closed = {};
    }
    
    
    $scope.getFlightsPages = function(){
        $scope.quantity = Object.keys($scope.flightsBySimilar).length;
        $scope.pages = Math.ceil($scope.quantity / 20);
        return $rootScope.getPages($scope.page, $scope.quantity);
    }
    
    
    $scope.selectFlight = function(i){
        var index = i;
        $rootScope.selectedFlight = $rootScope.flights.find[index];        
        $rootScope.selectedFlightIndex = i;
        $scope.showHotels();
    }
    
    
    $scope.showHotels = function(){
        $rootScope.view = 'hotels';             
    }           
});      