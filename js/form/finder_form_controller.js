'use strict';

// Инициализация; методы, доступные в каждом контроллере

var tourFinderApp = angular.module('tourFinderApp', ['ngCookies', 'ngAnimate', 'ngSanitize', 'ui.date', 'ui.dialog', 'rl.carousel','rl.mask', 'rl.pager','rl.cut','google-maps'], function($controllerProvider, $compileProvider, $provide) {
    providers = {
        $controllerProvider: $controllerProvider,
        $compileProvider: $compileProvider,
        $provide: $provide
    };    
});

tourFinderApp.config(['uiDateConfig', function(uiDateConfig) {
    angular.extend(uiDateConfig, {
        showButtonPanel:true,
        firstDay:1,
        dateFormat:'dd.mm.yy',
        nextText:'Следующий',
        prevText:'Предыдущий',
        closeText:'Закрыть',
        currentText:'Сегодня',
        dayNamesMin:['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayNames:['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        monthNames:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        changeYear: true,
        yearRange: "-100:+0"       
    });
}]);

tourFinderApp.constant('rootPath', finder_form_loader.root_path);
tourFinderApp.constant('payAgencyId', 13285);

tourFinderApp.run(function ($rootScope, $filter, $location, $timeout, $http, $sce, rootPath, payAgencyId) {  
           
    $rootScope.timeout = 99000;
    $rootScope.rootPath = rootPath;
    $rootScope.payAgencyId = payAgencyId;    
    $rootScope.search = {
         'stars[]':[], 
         'adults[]': [1],
         'children[]': [[]],
    };
    $rootScope.filter = {currency: null};
    $rootScope.classes = ['Эконом','Бизнес','Первый'];
    $rootScope.application = false;
    $rootScope.selectedHotel = false;
    $rootScope.spo_country = false;
    
    var path = $location.path();
    if(path){
        path = path.replace(/^\//,'',path);
        var data = path.match(/"\w+":"[a-z_\-]+"/g);        
        var item;
        if(data && data.length) for(var i=0; i<data.length; i++){
            item = data[i].match(/"(\w+)":"([a-z_\-]+)"/);
            if(item && item.length > 2){
                if(item[1] == 'form'){
                    var mapping = {
                        constructor:'constructor',
                        "excursion-tour":'spo',
                        excursion: 'excursions',
                        transfer: 'transfer',
                        visa:'visas'
                    };
                    if(typeof mapping[item[2]] != 'undefined') $rootScope.search.type = mapping[item[2]];                    
                }      
                if(item[1] == 'country') $rootScope.spo_country = item[2];     
            }
        }
        
    }    

    //счетчик времени для запросов поиска            
    $rootScope.count = function(){
         $rootScope.counter++;
         $rootScope.count_func = $timeout($rootScope.count,1000);
         if($rootScope.counter == 100 && !$rootScope.application){
             $rootScope.stopCount();
             if($rootScope.flights) $rootScope.hotels = {find: []};
             else $rootScope.flights = {find: []};
         }
    };
        
    
    $rootScope.stopCount = function(){        
         $rootScope.preload();
         $rootScope.time = $rootScope.counter;
         $rootScope.counter = 0;
         $timeout.cancel($rootScope.count_func);
    };       
    
    $rootScope.newSearch = function(){
         $rootScope.view = 'form';
         $rootScope.flights = false;
         $rootScope.hotels = false;
         $rootScope.counter = 0;
    };
    
    //разница дат в количестве ночей
    $rootScope.getDuration = function(x,y){
        if(typeof x == 'undefined') x = $rootScope.search.date_from;
        if(typeof y == 'undefined') y = $rootScope.search.date_to;
        var date_from = $.datepicker.parseDate("dd.mm.yy",  x);
        var date_to = $.datepicker.parseDate("dd.mm.yy",  y);
        var diff = Math.round((date_to - date_from)/1000/60/60/24);
        return diff;
    };        
    
    
    var getCountrySelectedPlace = function(selected_country_prop, search_prop){
        if(
                typeof $rootScope.selectedCountry == 'undefined' || 
                $rootScope.selectedCountry[selected_country_prop] == 'undefined'
        ) return;

        for(var i=0; i < $rootScope.selectedCountry[selected_country_prop].length; i++ ){
           if($rootScope.selectedCountry[selected_country_prop][i][0] == $rootScope.search[search_prop]) return $rootScope.selectedCountry[selected_country_prop][i][1];
        }
        return '';
    }
    
    
    $rootScope.getCityName = function(){
        return getCountrySelectedPlace('cities', 'living');
    } 
    
    
    $rootScope.getAirportName = function(){
        return getCountrySelectedPlace('airports', 'airport');
    }   
    
    $rootScope.getCurrencyName = function(){
         var currency = $rootScope.filter.currency;
         if(currency == 'RUR') currency = 'РУБ';         
         return currency;
    }
    
    //отображение прелоадера при вызове диалогов
    $rootScope.preload = function(show, wheel){
        if(typeof show == 'undefined') show = false;                
        if(typeof wheel == 'undefined') wheel = true;         
        var preloaders = $('#idFind div.findConstructor div.preloader, div.findConstructor div.preloader');
        if(show)
        {           
            preloaders.each(function(i,e){
                var parent = $(this).parent();
                $(this).width(parent.width());
                $(this).height(parent.height());
                $(this).css('display','block');
                $(this).css('top', (parent.position().top-parseInt(parent.css("border-top-width")))+'px');
                if(wheel) $(this).css('background-image', 'url('+rootPath+'img/loading.gif)');
                else $(this).css('background-image', 'none');
            });
        }   
        else
        {
            preloaders.css('display','none');
        }    
    }    

    
    $rootScope.newSearch();
    
    
    //данные для пейджера в списках отелей и перелетов
    $rootScope.getPages = function(current, quantity){
         var all = Math.ceil(quantity / 20);        
         var i;
         var result = [{label:1, current:current==0, key: 0, decor: false}];

         if(all > 1){
            if(current <=3){
                for(i=1; i<6; i++){ 
                    if(i < (all-1)) result.push({label:(i+1), current:current==i, key: i, decor: false});
                }
                if(all > 7) result.push({label:'..', current:false, key:false, decor: true});
                result.push({label:(all), current:current==(all-1), key: (all-1), decor: false});

            }
            else if(current >=(all-6)){
                if(current > 3 && all > 7) result.push({label:'..', current:false, key:false, decor: true});
                for(i=(all-6); i<all; i++){ 
                    if(i > 0) result.push({label:(i+1), current:current==i, key: i, decor: false});
                }             
            }
            else{
                if(all > 7) result.push({label:'..', current:false, key:false, decor: true});
                for(i=current-2; i<=current+2; i++){ 
                    if(i > 0 && i < (all-1)) result.push({label:(i+1), current:current==i, key: i, decor: false});
                }   
                if(all > 7) result.push({label:'..', current:false, key:false, decor: true});
                result.push({label:all, current:current==(all-1), key: (all-1), decor: false});
            }
         }
         return result;
     };
     
     
    $rootScope.getIndexForPage = function(i, page){
         return page*20+i;
     } 
     
     
    $rootScope.formatTime = function(minutes){
        var hours = Math.floor(parseInt(minutes)/60);
        var minutes = minutes - hours*60;
        return (hours ? hours+'ч.':'')+minutes+'м.';
    }     
    
    
    var findHotels = function(params){
         params.action = 'hotels';                             
         params.country = $rootScope.selectedCountry.key;         
         params.date_from = $rootScope.selectedFlight.origin[0][$rootScope.selectedFlight.origin[0].length-1].ArrivalDate;         
         params.date_to = $rootScope.selectedFlight.origin[1][0].DepartureDate;         
         params.duration = $rootScope.getDuration(params.date_from, params.date_to);
         params.currency = $rootScope.filter.currency;

         $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:params, timeout: $rootScope.timeout}).
         success(function(result, status, headers, config){
            $rootScope.stopCount();
            if(typeof(result.data)!= 'undefined'){
                $rootScope.hotels = result.data;  
                $rootScope.filter['star[]'] = [];
                $rootScope.filter['meal[]'] =[];  
                
                angular.forEach(($filter('orderBy')($rootScope.hotels.services.forfilter.star,'key')), function(v, k) { $rootScope.filter['star[]'].push(v.key.toString()) });                     
                angular.forEach(($filter('orderBy')($rootScope.hotels.services.forfilter.food,'label')), function(v, k) { $rootScope.filter['meal[]'].push(v.key) });     
                
                if(result.data.find.length) $rootScope.view = 'hotels';
                $("html,body").animate({scrollTop:   $('.finder_search').offset().top-10}, 1000);
            }
            else $rootScope.hotels = {find: []};
         }).
         error(function(data, status, headers, config) { $rootScope.stopCount(); });  
    }   


    $rootScope.startConstructorSearch = function(){
         if($rootScope.counter) return;         
         $rootScope.newSearch();
         $rootScope.count();
         
         var params = angular.copy($rootScope.search);
         params.currency = $rootScope.filter.currency;
         params.action = 'flights';
         $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:params, timeout: $rootScope.timeout}).
            success(function(response, status, headers, config) {               
                var result = response.data;
                if(
                        typeof result.find != 'undefined' && 
                        result.find.length /*&& 
                        $rootScope.orderType != 'ort'*/
                ){ 
                    for(var i=0; i < result.find.length; i++) result.find[i].select = i;
                    result.find = $filter('filter')(result.find, {oper: '!Чартер'});
                }
                $rootScope.flights = result;   
                if(result.find.length){                       
                    $rootScope.selectedFlight = result.find[0];
                    $rootScope.selectedFlightIndex = 0;
                    findHotels(params);
                }
                else{ 
                    $rootScope.stopCount();                 
                }
            }).
            error(function(data, status, headers, config) { $rootScope.stopCount(); });   
     }
     
    //от Виталика
    $rootScope.clearRtSources = function(){
        $('link[spo="transfer"]').remove();
	$('script[spo="transfer"]').remove();
	$('link[spo="excursions"]').remove();
	$('script[spo="excursions"]').remove();
    }
     
     
    $rootScope.startSpoSearch = function(){         
         if($rootScope.counter) return;     
         $rootScope.spo = false;
         $rootScope.types = []; 
         $rootScope.spo_cities = []; 
         $rootScope.spo_filtered = true;
         $rootScope.view = 'form';
         
         $rootScope.count();
         
         var params = {country: $rootScope.search.country, action: 'spo_list'};
         
         $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:params, timeout: $rootScope.timeout}).
            success(function(result, status, headers, config) {               
                if(
                    typeof(result.data) != 'undefined'
                ){ 
                    angular.forEach(result.data, function(value, key) {
                            result.data[key].price = parseInt(result.data[key].price);                                                                
                            result.data[key].name = $sce.trustAsHtml(value.name);
                            if($rootScope.types.indexOf(value.tip) == -1) $rootScope.types.push(value.tip);
                            if($rootScope.spo_cities.indexOf(value.town) == -1) $rootScope.spo_cities.push(value.town);
                    });
                    $rootScope.spo = result;
                    $rootScope.view = 'spo_list';
                }
                $rootScope.stopCount();  
            }).
            error(function(data, status, headers, config) { $rootScope.stopCount(); });   
     }
     
      
    $rootScope.dateInputsCheckRange = function(model, base){
         if(!model.date_from || !model.date_to) return;
         
        if($rootScope.getDuration(model.date_from, model.date_to) > 0) return;         
        var from = $.datepicker.parseDate("dd.mm.yy", model.date_from);  
        var to = $.datepicker.parseDate("dd.mm.yy", model.date_to);  
        switch(base){
            case 'from':
                to.setTime(from.getTime()+24*3600*1000);
                model.date_to = $.datepicker.formatDate('dd.mm.yy',to);
            break;
            case 'to':
                from.setTime(to.getTime()-24*3600*1000);
                model.date_from = $.datepicker.formatDate('dd.mm.yy',from);
            break;    
        }        
    }     
     
    
    $rootScope.latinOnly = function(e){
        var input = $(e.target).val();       
        input = input.replace(/[^a-z'"-]/i,'');
        $(e.target).val(input);
    }  
    
    
    $rootScope.cyrillicOnly = function(e){
        var input = $(e.target).val();       
        input = input.replace(/[a-z'"-]/i,'');
        $(e.target).val(input);
    }  
     
    
    $rootScope.transfer = function(){   
        var params = {
                        language: '', 
                        res: 'rt_spo_result', 
                        minimizeSearchForm: false,
                        theme: 'greyred'
                    };
        if($rootScope.spo_country) params.country = $rootScope.spo_country;    
        rt.widgetSpoTransfer('#rt_spo_rtSpoFindT',params);	
    }
    
    $rootScope.excursions = function(){                 
        var params = {
                        language: '', 
                        res: 'rt_spo_result', 
                        minimizeSearchForm: false,
                        theme: 'greyred'
                    };
        if($rootScope.spo_country) params.country = $rootScope.spo_country;
        rt.widgetSpoExcursion('#rt_spo_rtSpoFindE',params);
    }
    
    
    if(typeof $rootScope.search.type == 'undefined') $rootScope.search.type = 'constructor';
    switch($rootScope.search.type){
        case 'excursions':
            $rootScope.view = 'excursions_list';
            $rootScope.excursions();
        break;
        case 'transfer':
            $rootScope.view = 'transfer_list';
            $rootScope.transfer();         
        break;
        default:
            $rootScope.view = 'form';
        break;
    }
         
     
    $rootScope.waitStartForOrderInfoUrl = false;     
    
    $rootScope.orderType = 'ort';         
    if(
       typeof gde != 'undefined' && 
       gde.finder.clientParams.client != 'ort'
    ){
        $rootScope.orderType = 'client';
    }
});


    




