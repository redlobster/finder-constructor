//контроллер диалога отправки заявки в офис как электронного письма
tourFinderApp.controller('TourFinderOfficeOrderCtrl', function($scope, $rootScope, $http, $filter){
    $scope.data = {};
    if(typeof $scope.offices_data =='undefined') $scope.offices_data = false;
    var sender_div=false;
           
    $scope.selectOffice = function(i){        
        var order = angular.copy($rootScope.getOrder());
        order.office = $scope.offices_data.offices[ i ];
        order.office_city = ($filter('filter')($scope.offices_data.cities, {ID: order.sender.city}))[0].NAME;
        order.country =  $rootScope.selectedCountry.name;        
        order.city = ($filter('filter')($rootScope.cities, {key: $rootScope.search.city}))[0].name;        
        order.living = $rootScope.getCityName();
        order.airport = $rootScope.getAirportName()+' ('+$rootScope.search.airport+')';
        order.hotel = $rootScope.selectedHotel.name;
        order.departure = $rootScope.search.date_from;
        order.arrival =  $rootScope.search.date_to;
        order.duration = $rootScope.getDuration();
        order.touroper = $rootScope.selectedHotel.opername;
        order.aviaoper = $rootScope.selectedFlight.oper;
        order.rooms = [];        
        var rooms = $rootScope.getSelectedRoomsForHotel($rootScope.selectedHotelIndex);        
        for(var i=0; i < rooms.length; i++){
            order.rooms.push(rooms[i].roomname);
        }
        order.meal = rooms[0].mealname;
        order.currency = $rootScope.selectedHotel.currency;
        order.sum = $rootScope.calculateAll();
        order.sum_rur = $rootScope.calculateAll('RUR');
        order.rates = $rootScope.hotels.courses;
        
        for(var i=0; i < order.people.length; i++){
            order.people[i].visa = (order.people[i].visa == 'on' ? 1 : 0);
            order.people[i].visacity = $rootScope.hotels.getvisacost[order.people[i].visacity].cityname;
        }

        order = angular.toJson(order);     
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{action:'mail', order: order}})
        .success(function(data, status, headers, config){
            $scope.sended = data.success ? 'Сообщение отправлено' : 'Не удалось отправить сообщение';
            $rootScope.emptyOrder();
            $rootScope.handleOrderClose();            
        });
    }
    
    $scope.selectCity = function(){
        var params = {action: 'offices'};
        if(typeof $scope.data.city != 'undefined' && $scope.data.city) params['city'] = $scope.data.city;
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:params})
        .success(function(data, status, headers, config){
            if(!$scope.offices_data) $scope.offices_data = {};
            if(typeof data.cities != 'undefined') $scope.offices_data.cities = data.cities;
            if(typeof data.offices != 'undefined') $scope.offices_data.offices = data.offices;
        });
    }
    
    $rootScope.$on('chooseOffice', function (args) { 
        $scope.selectCity(); 
        $scope.sended = false;
    });
    
    
    
});
