//контроллер диалога бронирования экскурсионного тура
tourFinderApp.controller('TourFinderSpoOrderCtrl', function($scope, $rootScope, $http, $cookies, $filter, $timeout, $sce) { 
    
    $scope.plainName = function(){  
        if(typeof $rootScope.selectedSpo == 'undefined') return '';
        var name = $rootScope.selectedSpo.name.$$unwrapTrustedValue();
        var raw = name.match(/<b>(.*?)<\/b>/i);
        if(raw) name = raw[1];
        name = $sce.trustAsHtml(name);
        return name;
    }
    
    $scope.spo_order = {};
    
    $scope.getCities = function(){                            
        var cities = [];        
        angular.forEach($rootScope.spo.find, function(v, k) {
            if(cities.indexOf(v.town) == -1) cities.push(v.town);
        });   
        cities.sort();
        return cities;
    }
    
    $scope.submitOrder = function(){
        var data = angular.copy($scope.spo_order);
        data.country = $filter('filter')($rootScope.countries, {key: $rootScope.search.country})[0].name;
        data.spo_data = $rootScope.selectedSpo;
        var params = {
            action: 'book_spo',
            spo: data
        };
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK', {params:params, timeout: $rootScope.timeout})
        .success(function(result, status, headers, config){
            if(result.success){
                $scope.spo_hash = result.data.hash;
                $('#spo_order_hash input').val(result.data.hash);
                $('#spo_order_hash').submit();
            }
        });
    }    
});