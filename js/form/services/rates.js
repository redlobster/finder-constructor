tourFinderApp.factory('Rates', ['$http','rootPath', function($http, rootPath) {
  var self = this;
  self.rates = {
      rur: 1,
      labels: {usd:'$',eur:'€',gbp:'£',rur:'RUR'},      
      cyrilic: [{k: 'rur', v:'Рубль'},{k:'usd', v:'Доллар'},{k:'eur',v:'Евро'}, {k:'gbp',v:'Фунт'}],
      getLabel: function(currency){
          currency = currency.toLowerCase(); 
          return labels[currency];
      },
      convert: function(sum, from, to, with_currency){         
        if(typeof with_currency == 'undefined') with_currency = true; 
        sum = parseFloat(sum);  
        from = from.toLowerCase();  
        to = to.toLowerCase();  
        
        if(from == 'rub') from = 'rur';
        if(to == 'rub') to = 'rur';
        
        if(from == to) sum = Math.round(sum, 2);                    
        else sum = Math.round((sum * self.rates[from] / self.rates[to]), 2);
        
        return with_currency ? sum + ' ' + self.rates.labels[to] : sum;   
      },
      get: function(currency){ 
        currency = currency.toLowerCase();  
        return Math.round(parseFloat(self.rates[currency])*100)/100;
      }
  }
  
  $http.jsonp(rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{action: 'rates'}}).
    success(function(result, status, headers, config) {      
        angular.extend(self.rates, result.data); 
    }).
    error(function(data, status, headers, config) {
        if(typeof console != 'undefined') console.log("rates service error: " + status);
    });
  
  return self.rates;                                 
}]);