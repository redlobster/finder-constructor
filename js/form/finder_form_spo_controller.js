//контроллер формы экскурсиооных туров
tourFinderApp.controller('TourFinderSpoCtrl', ['$scope','$rootScope','$http','$cookies','$filter','$timeout','$sce','Rates',function($scope, $rootScope, $http, $cookies, $filter, $timeout, $sce, Rates) { 
    
    $scope.rates = Rates;
    $scope.countries = [];     
    $rootScope.types=[];
    $rootScope.spo_cities=[];
    
    var self = this;
    
    self.setCountries = function(){
        if(typeof $rootScope.spo_countries != 'undefined'){
            $scope.countries = $rootScope.spo_countries;
            
            if($rootScope.spo_country){
                $rootScope.search.country = $rootScope.spo_country;
                $rootScope.startSpoSearch();
            }
            else $rootScope.search.country = $scope.countries[0].key;            
        }
    }

    if(typeof($rootScope.spo_countries)== 'undefined'){
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{'action':'spo_countries'}, timeout: $rootScope.timeout}).
        success(function(result, status, headers, config){
            $rootScope.spo_countries = result.data;
            self.setCountries();
        });
    }   
    
    self.setCountries();                
     
     if(typeof($rootScope.spo_meal) == 'undefined'){ /// only for order
         var data = {action: "meal"};
         $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:data, timeout: $rootScope.timeout}).
         success(function(result, status, headers, config){       
            if(
                    result && 
                    typeof result.data != 'undefined' &&
                    result.data
            ) $rootScope.spo_meal = result.data;
         }).
         error(function(data, status, headers, config) {});  
     }
    
     $scope.selectCountry = function(){         
        var country = $filter('filter')($rootScope.countries, {key: $rootScope.search.country})[0];
        $cookies.selectedCountry = $rootScope.search.country;
        $rootScope.selectedCountry = country;                          
     }
     
     $scope.checkDates = function(base){         
        $rootScope.dateInputsCheckRange($rootScope.filter, base);         
     }
     
     $scope.getUniqueCities = function(){
         var cities = [];
         angular.forEach($rootScope.spo.find, function(v, k) {
             if(cities.indexOf(v.town) == -1) cities.push(v.town);
         });
         cities.sort();
         return cities;
     }     

     $rootScope.$watch('search.city', function(newValue, oldValue) {
         if(typeof(newValue) != 'undefined') $cookies.selectedCity = newValue;          
     });  
          
}]);