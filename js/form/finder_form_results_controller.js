// контроллекр элемента со списком результатов поиска
tourFinderApp.controller('TourFinderResultsCtrl', function($scope, $rootScope, $http, $filter, $timeout) {       
    $rootScope.hotels_filters = {food:{}, star:{}};    
    
    $scope.filterHotels = function(){ 
        $rootScope.preload(true);
        
        var params = $rootScope.filter;
        params.action = 'filter';
        params.sid = $rootScope.hotels.sessionid;
        $scope.page = 0;
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:params}).
        success(function(data, status, headers, config){
           $rootScope.preload();
           $rootScope.hotels = data.data;
           $rootScope.view = 'hotels';
        }).
        error(function(data, status, headers, config) { if(console != undefined) console.log('error'); });  
    }
    
    
    $rootScope.getPeopleQuantity = function(type){               
         var i, quantity = 0;
         if(typeof type == 'undefined') type=false;
         if(type != 'children') 
         {    
             for(i=0; i < $rootScope.search['adults[]'].length; i++) quantity += $rootScope.search['adults[]'][i];
         }
         if(type != 'adults')
         {
             for(i=0; i < $rootScope.search['children[]'].length; i++) quantity += $rootScope.search['children[]'][i].length; 
         }            
         return quantity;
    }
});