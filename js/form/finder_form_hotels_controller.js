//контроллер списка отелей
tourFinderApp.controller('TourFinderHotelsCtrl', function($scope, $rootScope, $http, $filter, $timeout, Rates) {  
    $scope.selectedRooms = [];
    $scope.sorting = 'pricemin';
    $scope.reverse = false;
    $scope.letters = [];
    $scope.letter = false;
    $scope.showMap = false;
    $scope.map = {
            center: false,
            zoom: 15
        }; 
    
    var handleHotels = function(hotels){   
        $scope.page = 0;
        $scope.selectedRooms = [];
        $scope.hiddenRooms = {};
        var i,j;
        if(hotels.find){//get first letters                                           
            for(i=0; i < hotels.find.length; i++){                        
                $scope.selectedRooms.push([]);
                var first = hotels.find[i].name[0];
                if($scope.letters.indexOf(first) == -1) $scope.letters.push(first);
                for(j=0; j < $rootScope.search['adults[]'].length ; j++) $scope.selectedRooms[i].push(0); 
            }
            $scope.letters.sort(); 
            $scope.pages_list = $scope.getHotelsPages();
            $rootScope.hotels_filters = hotels.services.forfilter; 
            if($scope.sorting != 'pricemin') $scope.getHotels();
         }
    }
    
    
    $rootScope.$watch('hotels', function(newValue, oldValue) {
        handleHotels(newValue);
    });   
    
    
    $rootScope.$watch('view', function(n,o){
        if(
            o == 'flights' && 
            n == 'hotels' && 
            typeof $rootScope.jumpTo != 'undefined'
        ){
            $timeout(function(){
                $(window).scrollTop($rootScope.jumpTo);        
            }, 400);
        }
    });
    
    
    $scope.byLetter = function(h){
        return !$scope.letter || h.name[0]==$scope.letter;
    }
    
    
    $scope.selectLetter = function(l){
        $scope.letter = l ? l : false;
        $scope.toPage(0);
        
    }

    
    $scope.changeSorting = function(s){
        if(s == $scope.sorting) $scope.reverse = !$scope.reverse;
        else $scope.reverse = false;        
        $scope.sorting = s;
        $scope.getHotels();
    }


    $scope.getSorting = function(h){
        switch($scope.sorting){
            case 'pricemin':   return parseInt(h.pricemin);
            break;
            case 'name':       return h.name;
            break;
            case 'starcode':   return parseInt(h.starcode);
            break;    
        }
    }               
    
    
    $scope.getHotels = function(){
        $rootScope.hotels.find = $filter('orderBy')($rootScope.hotels.find, $scope.getSorting, $scope.reverse);
        return $rootScope.hotels.find;
    }
    

    $scope.Order = function(i){
        $rootScope.selectedHotelIndex = $scope.getHotelIndexForPage(i);
        $rootScope.selectedHotel = $rootScope.hotels.find[ $rootScope.selectedHotelIndex ];
        $rootScope.application=true;   
        $rootScope.preload(true,false);
        $rootScope.$broadcast('order'); 
    }     
     
     
    $scope.handleDialogClose = function() {
         $scope.dialogOpen = false;         
         $rootScope.preload();
    }
    
    $scope.handleMapDialogClose = function() {
         $scope.showMap = false;         
         $scope.map.center = false;
         $rootScope.preload();
    }
    
    $scope.openMap = function(hotel){      
        $scope.mapHotelname = hotel.name;
        $scope.showMap = true; 
        $rootScope.preload(true,false);
        $timeout(function(){
            $scope.map.center = {
                latitude: parseFloat(hotel.lat),
                longitude: parseFloat(hotel.lon)
            }}, 300);
    }
    
    $rootScope.handleOrderClose = function(){
        $rootScope.clearOrder(true); 
        $rootScope.selectedHotel = false;
        $rootScope.application = false;
        $rootScope.preload();
    }


    $scope.FlightData = function(){
        $scope.dialogOpen = true;
        $rootScope.preload(true,false);
    }


    $scope.ChangeFlight = function(hotel_index){
        $rootScope.jumpTo = $('.finder_result_item').eq(hotel_index).offset().top-12;
        $rootScope.view = 'flights';
    }


    $scope.toPage = function(page){
        $scope.page = page;
        $scope.pages_list = $scope.getHotelsPages();
    }


    $scope.getHotelIndexForPage = function(i){
        return $rootScope.getIndexForPage(i, $scope.page);
    }


    $scope.getHotelsPages = function(){
        if($scope.letter) $scope.quantity = $filter('filter')($rootScope.hotels.find, $scope.byLetter, false).length;
        else $scope.quantity = $rootScope.hotels.find.length;
        $scope.pages = Math.ceil($scope.quantity / 20);
        return $rootScope.getPages($scope.page, $scope.quantity);
    }          


    $rootScope.calculateFlight = function(currency){
        if(typeof currency == 'undefined') currency = $rootScope.filter.currency;            
        return Rates.convert($rootScope.selectedFlight.sum,'RUR',currency,false);
    }


    var getRoomIndex = function(room){
        var adults, children, indexes= [];
        for(var i=0; i < $rootScope.search['adults[]'].length; i++){
            adults = $rootScope.search['adults[]'][i];
            children = $rootScope.search['children[]'][i].length;
            if(adults == room.adultcount && children == room.childcount) indexes.push(i); 
        }
        return indexes;
    }


    var getRoomsByNumbersForIndex = function(index){
        var result = [];
        if(typeof $rootScope.hotels.find[index].roomsByNumber != 'undefined') return $rootScope.hotels.find[index].roomsByNumber;

        var rooms = $rootScope.hotels.find[index].room;
        var i, j, indexes;

        for(i=0; i < $rootScope.search['adults[]'].length; i++) result.push([]);         
        for(var i in rooms){ 
            indexes = getRoomIndex(rooms[i]);
            for(j=0; j < indexes.length; j++) result[indexes[j]].push(rooms[i]);                          
        }   
        $rootScope.hotels.find[index].roomsByNumber = result;
        return result;
    }


    $scope.getRoomsByNumbers = function(k){         
        var index = $scope.getHotelIndexForPage(k);
        return getRoomsByNumbersForIndex(index);
    }
    
    
    $rootScope.getRoomAdultChildren = function(i){
        var content = $rootScope.search['adults[]'][i]+'взр. + '+$rootScope.search['children[]'][i].length+'дет.';
        if($rootScope.search['children[]'][i].length){
            content += '(возр. ';
            content += $rootScope.search['children[]'][i].join(',');
            content += ')';
        }
        return content;
    }


    $rootScope.getSelectedRoomsForHotel = function(i){
        var index;                  
        var rooms = getRoomsByNumbersForIndex(i);
        if(!rooms || !rooms[0] || !rooms[0].length) return 0;
        //rooms комнаты рассортированы по принадлежности к комнатам поиска

        var result = [];
        for(var j=0; j < rooms.length; j++){
            //ищем индекс выбранного варианта для текущего номера поиска
            if(typeof $scope.selectedRooms[i] == 'undefined') continue;
            if(typeof $scope.selectedRooms[i][j] == 'undefined') index = 0;
            else index = $scope.selectedRooms[i][j];
            result.push( rooms[j][index] );
        }    
        return result;
    }


    $scope.calculateHotelRoomsPrice = function(i, currency){      
        i = $scope.getHotelIndexForPage(i);
        if(typeof currency == 'undefined') currency = $rootScope.filter.currency;  

        var rooms = $rootScope.getSelectedRoomsForHotel(i);
        var sum=0;
        for(var j=0; j < rooms.length; j++){   
           sum += Rates.convert(rooms[j].price, rooms[j].currency, currency, false);
        }               
        return sum;
    }     


    $rootScope.calculateHotelFlight = function(i, currency){
        if(typeof currency == 'undefined') currency = $rootScope.filter.currency;  
        var sum = $scope.calculateFlight(currency);
        sum += $scope.calculateHotelRoomsPrice(i, currency);    
        return sum;
    }      
});