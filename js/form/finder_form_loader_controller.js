//контроллер preloader а и его сообщений
tourFinderApp.controller('TourFinderLoaderCtrl', function($scope, $rootScope, $http, $filter, $timeout){   
    $rootScope.$watch('counter', function(n, o) { 
        $rootScope.loaderMessage = false;    
        if($rootScope.view == 'form'){
            if(
                    $rootScope.search.type == 'spo' && 
                    !$rootScope.spo 
                    && parseInt(n)
            ){
                $rootScope.loaderMessage = 'Поиск тура.';
            }
            else if(
                    !$rootScope.flights || 
                    typeof $rootScope.flights.find == 'undefined'
            ){
                if(parseInt(n)) $rootScope.loaderMessage = 'Поиск авиаперелета.';
            }
            else{
                if($rootScope.flights.find.length && parseInt(n)) $rootScope.loaderMessage = 'Авиаперелет с минимальной ценой найден, поиск отеля.';
                else if(!$rootScope.flights.find.length) $rootScope.loaderMessage = 'Авиаперелет не найден, измените параметры поиска.';
                if(
                    $rootScope.flights.find.length && !parseInt(n) &&
                    (!$rootScope.hotels || !$rootScope.hotels.find.length)
                ) $rootScope.loaderMessage = 'Отель не найден, измените параметры поиска';                                        
            }
        }
        else if($rootScope.application){
            if(
                    $rootScope.reserveHotel && 
                    !$rootScope.reserveHotel.error.length && 
                    parseInt(n)
            ) $rootScope.loaderMessage = 'Оформление заказа'; 
                       
            else if(
                $rootScope.orderPassport && 
                $rootScope.orderPassport.error.length
            ){ 
                $rootScope.loaderMessage = $rootScope.orderPassport.error;
            }
            
             else if((
                    !$rootScope.orderPassport || 
                    !$rootScope.reserveFlight
            ) && parseInt(n)) $rootScope.loaderMessage = 'Резервирование авиаперелета.';
            
            else if(parseInt(n) && $rootScope.reserveFlight) $rootScope.loaderMessage = 'Резервирование гостиницы.';
            else if($rootScope.reserveFlight && $rootScope.reserveFlight.error.length){ 
                $rootScope.loaderMessage = $rootScope.reserveFlight.error;
            }                
            else if($rootScope.reserveHotel && $rootScope.reserveHotel.error.length) $rootScope.loaderMessage = $rootScope.reserveHotel.error;
        }
    });    
});