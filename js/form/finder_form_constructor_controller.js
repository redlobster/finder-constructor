// Контроллер для формы отель+ перелет

tourFinderApp.controller('TourFinderConstructorCtrl', function($scope, $rootScope, Rates) {     
    $scope.children = [0];          
    $scope.rates = Rates;      
    
    $scope.addRoom = function(){
        if($rootScope.search['adults[]'].length < 3){
            $rootScope.search['adults[]'].push(1);
            $rootScope.search['children[]'].push([]);
            $scope.children.push(0);
        }
    }

    $scope.removeRoom = function(i){
        $rootScope.search['adults[]'].splice(i,1);
        $rootScope.search['children[]'].splice(i,1);
        $scope.children.splice(i,1);
    }
    
    $scope.setAges = function(j){
        $rootScope.search['children[]'][j] = [];
        for(var i=0; i < $scope.children[j]; i++){
            $rootScope.search['children[]'][j].push(0);
        }
    }  
});