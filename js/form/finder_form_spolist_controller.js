//контроллер списка экскурсионных туров
tourFinderApp.controller('TourFinderSpoListCtrl', function($scope, $rootScope, $http, $cookies, $filter, $timeout, $sce, Rates) {     
    $scope.rates = Rates;        
    
    $scope.getFilteredList = function(){
        $rootScope.spo_filtered = false;
        var items = $rootScope.spo.data;
        if($rootScope.filter.spotype != $rootScope.spo_default_type) items = $filter('filter')(items, {tip: $rootScope.filter.spotype}, true);
        if($rootScope.filter.town != $rootScope.spo_default_town) items = $filter('filter')(items, {town: $rootScope.filter.town}, true);
        if($rootScope.filter.date_from){ 
            items = $filter('filter')(items, {end: $rootScope.filter.date_from}, function(end, date_from){
                return $rootScope.getDuration(date_from, end)>=0;
            });
        }
        if($rootScope.filter.date_to){
            items = $filter('filter')(items, {begin: $rootScope.filter.date_to}, function(begin, date_to){ 
                return $rootScope.getDuration(begin, date_to)>=0;
            });
        }
        $rootScope.spo_filtered = items.length ? true : false;
        return items;
    }   
    
    //вызов диалога бронирования, у него свой контроллер
    $scope.brone = function(index){
        $scope.broneDialog = true;
        $rootScope.selectedSpo = $rootScope.spo.data[index];
    }
    
    
    $scope.handleSpoOrderClose = function(){
        $scope.broneDialog = false;
    }
    
    
    $scope.handleTourClose = function(){
        $scope.tourOpen = false;
    }
    
    
    
});