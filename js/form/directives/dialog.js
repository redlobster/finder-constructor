
angular.module('ui.dialog', [])
.directive('uiDialog', function ($timeout) {
    return {
        scope: { 
            okButton: '@',
            okCallback: '=',
            cancelButton: '@',
            cancelCallback: '=',
            open: '@',
            title: '@',
            width: '@',
            height: '@',
            modal: '@',
            show: '@',
            hide: '@',
            autoOpen: '@',
            resizable: '@',
            closeOnEscape: '@',
            hideCloseButton: '@',
            handleClose: '='
        },
        replace:false,
        transclude: true,    // transclusion allows for the dialog 
                              // contents to use angular {{}}
        template: '<div ng-transclude></div>',      // the transcluded content 
                                                    //is placed in the div
        link: function (scope, element, attrs) {
            // Close button is hidden by default
            var hideCloseButton = attrs.hideCloseButton || true;
            
            // Specify the options for the dialog
            var dialogOptions = {
                autoOpen: attrs.autoOpen || false,
                title: attrs.title,
                width: attrs.width || 350,
                height: attrs.height || 200, 
                modal: attrs.modal || false,
                show: attrs.show || '',
                hide: attrs.hide || '',
                draggable: attrs.draggable || true,
                resizable: attrs.resizable,
                closeOnEscape: attrs.closeOnEscape || false,
                
                close: function() {
                    scope.handleClose();
                     $timeout(function() {
                        scope.$apply(scope.handleClose());
                    },500);
                }
            };
           
           // Add the buttons 
           dialogOptions['buttons'] = [];
           
           if(attrs.okButton) {
               var btnOptions = { 
                   text: attrs.okButton, 
                   click: function() { scope.$apply(scope.okCallback()); }
               };
               dialogOptions['buttons'].push(btnOptions);    
           }
           
            if(attrs.cancelButton) {
               var btnOptions = { 
                   text: attrs.cancelButton, 
                   click: function() { scope.$apply(scope.cancelCallback()); }
               };
               dialogOptions['buttons'].push(btnOptions);    
           }
           
           // Initialize the element as a dialog
           // For some reason this timeout is required, otherwise it doesn't work
           // for more than one dialog
           /*
           $timeout(function() {*/
               $(element).dialog(dialogOptions);
               var dialog_holder = $(element).closest('.ui-dialog').eq(0);
               dialog_holder.css('z-index','100');
               var close_icon = dialog_holder.find('button.ui-dialog-titlebar-close span.ui-icon-closethick').eq(0);
               close_icon.css('left', '-2%');
               close_icon.css('top', '-2%');
           /*},0);*/
            
            // This works when observing an interpolated attribute
            // e.g {{dialogOpen}}.  In this case the val is always a string and so
            // must be compared with the string 'true' and not a boolean
            // using open: '@' and open="{{dialogOpen}}"
            
            attrs.$observe('open', function(val) {
                if (val == 'true') {
                    $(element).dialog("open");
                } 
                else 
                {
                    //console.log('close');
                    $(element).dialog("close");
                    
                }
            });
            
            // This allows title to be bound
            attrs.$observe('title', function(val) {
                //console.log('observing title: val=' + val);
                $(element).dialog("option", "title", val);                   
            });
        } 
    }
});  
    