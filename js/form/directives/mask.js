angular.module('rl.mask', [])
.directive('jqueryMask', function () {
    return {
        restrict: 'AEC',
        link: function (scope, element, attr) {
            try{
                element.mask(attr.jqueryMask);
            }
            catch(e){
                
            }
        }
    }
});