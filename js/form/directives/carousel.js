angular.module('rl.carousel', [])
.directive('onNgRepeatFinish', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
})
.directive('rlCarousel', function($timeout, rootPath) {
    return {
        restrict: 'E',
        replace: true,
        compile: function($scope,element, attrs) {
            return {
              post: function($scope,element, attrs) {
                $scope.images = angular.fromJson(attrs.images);
                $scope.$on('ngRepeatFinished', function(e) {
                     $(element).slides({
                             preload: /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()) ? false : true,
                             preloadImage: 'http://hotel.ross-tur.ru/img/preloader2.gif',
                             play: 5000,
                             pause: 2500,
                             hoverPause: true,
                             container: 'rt-slides-container',
                             next:'rt-next',
                             prev:'rt-prev',
                             pagination:false,
                             generateNextPrev:false,
                             generatePagination:false,
                             prependPagination:false
                     });
                });
              }
            };
        },
    }
});