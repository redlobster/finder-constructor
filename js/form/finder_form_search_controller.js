// контроллер общий для элемента с формами
tourFinderApp.controller('TourFinderSearchCtrl', function($scope, $rootScope, $http, $cookies, $filter, $timeout, $sce) { 
        
     $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{action: 'geo'}}).
        success(function(result, status, headers, config) {                                    
            if(!result.success){
                console.log('geo error');
                return;
            }
            result = result.data;
            $rootScope.countries = result.country;
            $rootScope.cities = result.city;
            $scope.city_codes = result.city_codes;
            $rootScope.search.city = (typeof $cookies.selectedCity == 'undefined' ? 'MOW' : $cookies.selectedCity); 
            $rootScope.selectedCountry = (
                    $cookies.selectedCountry == undefined ? (result.country_codes.cz == undefined ? result.country[0] : result.country[result.country_codes.cz]) : result.country[result.country_codes[$cookies.selectedCountry]]);                               
            if(
                    $rootScope.selectedCountry.cities!= undefined && 
                    $rootScope.selectedCountry.cities.length
            ) $rootScope.search.living = $rootScope.selectedCountry.cities[0][0];
    
            if(
                    $rootScope.selectedCountry.airports != undefined && 
                    $rootScope.selectedCountry.airports.length
            ) $rootScope.search.airport= $rootScope.selectedCountry.airports[0][0];          
            
            if(
                    typeof gde != 'undefined' && 
                    typeof gde.finder != 'undefined' && 
                    typeof gde.finder.loading != 'undefined'
            ) gde.finder.loading();
        }).
        error(function(data, status, headers, config) { if(console != undefined) console.log('geo error'); });   


     $scope.getDateAdjustedByDays = function(days){
        return $filter('date')(new Date(new Date().getTime()+days*24*3600*1000),'dd.MM.yyyy');
     }


     $scope.selectCountry = function(){         
        var country = $filter('filter')($rootScope.countries, {key: $rootScope.search.country})[0];
        $cookies.selectedCountry = $rootScope.search.country;
        $rootScope.selectedCountry = country;        
         
        if(country.cities != undefined && country.cities.length){
            $rootScope.search.living = country.cities[0][0];
        }
 
        if(country.airports != undefined && country.airports.length){
            $rootScope.search.airport = country.airports[0][0];
        }        
     }
     
     
     $rootScope.getSelectedCityName = function(){
         return $rootScope.cities[$scope.city_codes[$rootScope.search.city]].name;
     }


     $rootScope.$watch('search.city', function(newValue, oldValue) {
         if(typeof(newValue) != 'undefined') $cookies.selectedCity = newValue;          
     });

                  
     $scope.checkDates = function(base){
         if($rootScope.getDuration() > 0) return;
         var from = $.datepicker.parseDate("dd.mm.yy", $rootScope.search.date_from);  
         var to = $.datepicker.parseDate("dd.mm.yy", $rootScope.search.date_to);  
         switch(base){
             case 'from':
                 to.setTime(from.getTime()+24*3600*1000);
                 $rootScope.search.date_to = $.datepicker.formatDate('dd.mm.yy',to);
             break;
             case 'to':
                 from.setTime(to.getTime()-24*3600*1000);
                 $rootScope.search.date_from = $.datepicker.formatDate('dd.mm.yy',from);
             break;    
         }
     }
    
});