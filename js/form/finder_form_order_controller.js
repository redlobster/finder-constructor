//контроллер диалога бронирования отель+перелет
tourFinderApp.controller('TourFinderOrderCtrl', function($scope, $rootScope, $http, $filter, $timeout, Rates) {
        
    $rootScope.clearOrder = function(){
            $rootScope.orderInit = false;    
            $rootScope.orderPassport = false;
            $rootScope.reserveFlight = false;
            $rootScope.reserveHotel = false;
            $scope.cancellation = false;
            $scope.countryVisa = false;
            $rootScope.needConfirmation = false;
    }
    
    $rootScope.emptyOrder = function(){
        $scope.order = {people:[], comment: '', bonuscard: ''};  
            for(var i=0; i < $rootScope.getPeopleQuantity();i++) $scope.order.people.push({
                lastname:'', 
                firstname: '', 
                passport: '', 
                passport_date: '', 
                gender: 'm', 
                birth: '', 
                insurance: 0, 
                visa: 0, 
                visacity:''
        });
    }
    
    $rootScope.clearOrder();            
    
    var initOrder = function(){
        if(typeof $scope.order == 'undefined'){ 
             $rootScope.emptyOrder();
        }
        else{
            angular.forEach($scope.order.people, function(value, key){
               $scope.order.people[key].insurance = 0;
               $scope.order.people[key].visa = 0;
               $scope.order.people[key].visacity = '';
            });
        }
    }
    
    
    var visaInit = function(){        
        if(!$rootScope.orderInit){
            $scope.visaAgreement = false;
            $scope.rooms = $rootScope.getSelectedRoomsForHotel($rootScope.selectedHotelIndex);
            $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{action: 'visa'}}).
            success(function(data, status, headers, config){    
                $rootScope.orderInit = data.data;  
                $scope.countryVisa = getVisaCountry();
            }).
            error(function(data, status, headers, config) { if(console != undefined) console.log('error'); });  
        }         
    }
    visaInit();
    
    
    $scope.chooseOffice = false;   
    
    
    $rootScope.$watch('flights', function(newValue, oldValue){  initOrder(); });
    
    
    $rootScope.$watch('view', function(newValue, oldValue){
        if(newValue != 'hotels') $rootScope.application = false;
    });
    
    
    $rootScope.$on('order',function(){ visaInit(); });    
    
    
    $scope.getLivingTerm = function(){
        var start = $rootScope.selectedFlight.origin[0][$rootScope.selectedFlight.origin[0].length-1].ArrivalDate;
        var stop = $rootScope.selectedFlight.origin[1][0].DepartureDate;
        var nights = $rootScope.getDuration(start, stop);
        if(nights >=11 && nights >=14) return nights + ' ночей';
        else if(nights % 10 == 1) return nights + ' ночь';
        else if(nights % 10 >= 2 && nights % 10 <= 4) return nights + ' ночи';
        else return nights + ' ночей';
    }
    
    
    var getInsuranceBase = function(currency){
        var base = parseInt($scope.getLivingTerm())+1;            
        var sum = base * ($rootScope.selectedHotel.currency == 'RUR' ? 30 : 1);                
        return Rates.convert(sum,$rootScope.selectedHotel.currency,currency,false);
    }
    
    
    var getInsuranceMultiple = function(age){
        var m = 1;
        if(age > 64  && age <= 79) m = 2;
        else if(age > 79 ) m = 4;
	return m * parseInt($rootScope.hotels.countrydata[4]);	
    }
    
    
    $scope.calculateInsuranceVisas = function(currency, by_type){
        if(typeof by_type == 'undefined') by_type = false;
        var sum = 0;
        var age, visa, cost;
        var insurance = getInsuranceBase(currency);
        if($scope.order == undefined) initOrder();
        for(var i = 0; i < $scope.order.people.length; i++){
            if($scope.order.people[i].birth){
                age = $scope.getAge($scope.order.people[i].birth);
            }
            else age = 100;                        
            //calculate insurance
            if($scope.order.people[i].insurance && by_type!= 'visa') sum += insurance*getInsuranceMultiple(age);            
            //calculate visa
            if($scope.order.people[i].visa && by_type!= 'insurance'){    
                visa = $rootScope.hotels.getvisacost[ $scope.order.people[i].visacity ];
                cost = (age > visa.child_age ? visa.adult_sum : visa.child_sum);        
                sum += Rates.convert(cost,visa.valute,currency,false);                
            }
        }                     
        return sum;
    }
    
    $rootScope.calculateAll = function(currency){
        if(typeof currency == 'undefined') currency = $rootScope.selectedHotel.currency;
        return $scope.calculateInsuranceVisas(currency) + $rootScope.calculateHotelFlight($rootScope.selectedHotelIndex, currency);
    }    
    
    
    $scope.calculateHotelFlightInsuranceVisa = function(){ 
        try{
            $scope.sum_hotel_currency = $rootScope.calculateAll();
            $scope.sum_hotel_rur = $rootScope.calculateAll('RUR');
        }
        catch(e){}
    }
    $scope.calculateHotelFlightInsuranceVisa(); 
                
        
    $rootScope.$watch('selectedHotel', function(newValue, oldValue) {         
        $scope.calculateHotelFlightInsuranceVisa();        
        if(typeof $scope.rooms == 'undefined') $scope.rooms = $rootScope.getSelectedRoomsForHotel($rootScope.selectedHotelIndex);
        var room_ind = {};
        $.each( $scope.rooms, function(i,v){
            room_ind[i+1] = v.room_ind;
        })
        
        var params = {            
            currency: $rootScope.selectedHotel.currency,
            hotel_ind: $rootScope.selectedHotel.hotel_ind,
            oper: $rootScope.selectedHotel.oper,
            room_ind: room_ind,
            sessionid: $rootScope.hotels.sessionid
        }        
        params = angular.toJson(params);
        
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params: {data: params, action: 'cancellation'}} ).
            success(function(data, status, headers, config){                    
                if(data && typeof(data.data.info.LastDayWithoutPenalityValue)!='undefined'
                ){
                    $scope.cancellation = data.data.info;
                }            
                else $scope.cancellation = 'Наличие мест и стоимость размещения в этом отеле автоматически подтвердить невозможно. Просьба связаться с менеджером.';    
            }).
            error(function(data, status, headers, config) { if(console != undefined) console.log('error'); });  
        }               
    );               
    
    
    $scope.getAge = function(birth){
        var comeback, comeback_year, birth_year, years, this_year_birthday;   
        try{ 
            birth = $.datepicker.parseDate("dd.mm.yy", birth);            
        }
        catch(e){
            return 100;
        }
        birth = new Date(birth);
        birth_year = birth.getFullYear();
        try{
            comeback = $.datepicker.parseDate("dd.mm.yy", $rootScope.search.date_to);
        }
        catch(e){
            return 100;
        }
        comeback = new Date(comeback);
        comeback_year = comeback.getFullYear();

        years = comeback_year - birth_year;            
        //если в этом году днюха после возвращения, то возраст на год меньше
        this_year_birthday = new Date(comeback_year, birth.getMonth(), birth.getDate());
        if(this_year_birthday.getTime() > comeback.getTime()) years--;
        return years;          
    }
    
    
    $scope.getPeopleIndex = function(room, index){
        var p=0;         
        for(var i=0; i < room; i++){
            p += $rootScope.search['adults[]'][i] + $rootScope.search['children[]'][i].length;
        }
        p += index;
        return p;
    }
        
    
    var getVisaCountry = function(){
        
        for(var i=0; i < $rootScope.orderInit.visa.length; i++){
            if($rootScope.selectedCountry.name == $rootScope.orderInit.visa[i][2]) return $rootScope.orderInit.visa[i];
        }
        return false;
    }    
    
    
    var validateForm = function(){   
        var valid = true;
        var message = '';
        
        angular.forEach($scope.order.people, function(value, key){
            if(valid){                
                if(!(value.firstname && value.lastname && value.birth)){
                    valid=false;
                    message = 'Фамилию, имя и дату рождения'
                }
                if(!(value.passport && value.passport_date)){
                    valid=false;
                    message += (message ? ', п' : 'П')+'аспорт или другой документ';
                }
                if(message) message= message +' нужно указать';
            }            
        });
        
        if($rootScope.orderType != 'ort'){
            if(!$scope.order.sender || !$scope.order.phone || !$scope.order.email){
                valid=false;
                message += (message ? ', также н': 'Н')+'еобходимо указать все данные контактного лица для обработки заявки';
            }
        }
        
        if(message){
            message += '.';
            alert(message);
        }
        return valid;
    }


    var getOrderRegistrationInfo = function(){
        var flight_sid = $rootScope.flights.sid;
        var hotel_sid = ($rootScope.selectedHotel.sessionid == undefined ? $rootScope.hotels.sessionid : $rootScope.selectedHotel.sessionid);

        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{action: 'get_order', hotel_sid: hotel_sid, flight_sid: flight_sid}}).
        success(
            function(data, status, headers, config){                
                if(data.success == 1) window.location.replace(data.url);                 
                else if($rootScope.counter < 180) $scope.registration_func = $timeout(getOrderRegistrationInfo,3000);                
                else{                                     
                    $timeout.cancel($scope.registration_func);                   
                    $rootScope.stopCount();                    
                }                
            }
        ).
        error(function(data, status, headers, config) { 
            $rootScope.waitStartForOrderInfoUrl = false;
            $rootScope.stopCount();
            if(typeof console != 'undefined') console.log('order send post error'); 
        });   
    }
    
    
    var submitForm = function(){
        $('.finder_order form').eq(0).submit();        
    }
    
    
    $scope.handleOfficesDialogClose = function(){
        $scope.chooseOffice = false;
    }
           
    
    var handleReservationHotel = function(){
        var params = getPayObject();

        params = angular.toJson(params);
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{action: 'reserve_hotel', order: params}}).
        success(function(data, status, headers, config){                                                     
                 $rootScope.reserveHotel = (data && typeof data.data != 'undefined' ? data.data: false);
                 $rootScope.stopCount();
                 if(data && typeof data.data != 'undefined' && typeof data.data.orderid != 'undefined' && data.data.orderid){     
                    $rootScope.reserveHotel.error = false;
                    submitForm();
                 }  
                 else if(typeof data.data.error != 'undefined' && data.data.error)
                 {
                     $rootScope.reserveHotel.error = data.data.writeorder;
                 }
                 else{
                    $rootScope.reserveHotel.error = 'Не удалось забронировать гостиницу. Выберите другой вариант или свяжитесь с менеджером.'
                 }
        }).
        error(function(data, status, headers, config) { if(console != undefined) console.log('error'); });  
    }                        
    
    
    var handleReservationFlight = function(){
        if(!$rootScope.counter) $rootScope.count();
        var params = angular.copy($scope.order);            
        params['HotelAndAviaSum'] = $rootScope.calculateHotelFlight($rootScope.selectedHotelIndex, $rootScope.selectedHotel.currency);
        params['rooms'] = $rootScope.search['adults[]'].length;       
        params['sid'] = $rootScope.flights.sid;
        params['select'] = $rootScope.selectedFlight.select;
        
        params.comment += ' \n '+getInsuranceDataAsText();
        
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{action: 'reserve_flight', order: angular.toJson(params)}}).
        success(function(data, status, headers, config){                                     
            $rootScope.reserveFlight = (!data || typeof data.data == 'undefined' ? {} : data.data); 
            if(
                    typeof $rootScope.reserveFlight.CustomerIdentifier != 'undefined' && 
                    $rootScope.reserveFlight.CustomerIdentifier && 
                    !$rootScope.reserveFlight.error
            ){               
               handleReservationHotel();
            }
            else{
                $rootScope.stopCount();
                if(typeof $rootScope.reserveFlight.error == 'undefined'){
                    $rootScope.reserveFlight.error = 'Не удалось забронировать авиаперелет. Выберите другой.';
                }                
            }
        }).
        error(function(data, status, headers, config) { if(console != undefined) console.log('error'); });  
    };            
    
    
    $scope.checkPassportData = function(i){  
        $rootScope.orderPassport = false;
        $rootScope.needConfirmation = false;
                        
        if(typeof($scope.order.people[i]) == 'undefined'){
            return handleReservationFlight();            
        }  
        
        var params = {
            action: 'passport',
            country: $scope.countryVisa ? $scope.countryVisa[1] : '',
            departure:$rootScope.selectedFlight.origin[1][0].DepartureDate,
            arrival: $rootScope.selectedFlight.origin[0][$rootScope.selectedFlight.origin[0].length-1].ArrivalDate,
            passport: $scope.order.people[i].passport_date
        };
        
        
        if(!$rootScope.counter) $rootScope.count();
        
        $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:params}).
        success(function(response, status, headers, config){ 
            
            var data = response.data;
            $rootScope.orderPassport = data; 
            if(data && data.status == 1){              
               $scope.checkPassportData(i+1);               
            }
            else{
               $rootScope.stopCount();
               $rootScope.needConfirmation = i+1;
               $rootScope.orderPassport.error = '\
                Данные паспорта \
                '+($scope.order.people[i].firstname+' '+$scope.order.people[i].lastname+' '+$scope.order.people[i].passport).toUpperCase()+'\
                не верны или срок его действия закончится раньше положенного срока.';
            }
        }).
        error(function(data, status, headers, config) { if(console != undefined) console.log('error'); });  
    }
    
        
    $scope.sendOrder = function(pay){
        if(!validateForm()) return;
        switch(pay){
            case 'ort':
                $scope.checkPassportData(0);
            break;
            case 'pay':
                $scope.checkPassportData(0);
            break;
            case 'agency':
                $scope.isAgencyOrder=true;
            break;
            case 'office':
                $scope.chooseOffice = true;   
                $rootScope.$broadcast('chooseOffice', {});
            break;                
        }           
    };

    
    $scope.findVisaCity = function(){
        var city = $rootScope.getSelectedCityName();
        for(var i in $rootScope.hotels.getvisacost){
           if($rootScope.hotels.getvisacost[i].cityname == city) return i;           
        }        
        return 0;
    }
        
    
    $scope.getCollectionForRoom = function(room){
        var strange_collection = [];
        for(var i=0; i < (parseInt(room.adultcount) + parseInt(room.childcount)); i++) strange_collection.push(i+1);
        return strange_collection;
    }  
   
    
    $scope.checkVisaAgreement = function(i){
        if(!$scope.visaAgreement && $scope.order.people[i].visa) $scope.visaAgreementDialogOpen = true;       
    }
    
    
    $scope.handleVisaAgreementDialogClose = function() {        
         $scope.visaAgreementDialogOpen = false;
    }
    
    
    $scope.visaAgreementConfirm = function(){
        $scope.visaAgreement = true;
        $scope.visaAgreementDialogOpen = false;
    }
    
    
    $scope.visaAgreementReject = function(){
        $scope.handleVisaAgreementDialogClose();
        for(var i=0; i < $scope.order.people.length; i++) $scope.order.people[i].visa = false;
        $scope.calculateHotelFlightInsuranceVisa();
    }
    
    
    
    
    
    $scope.handleIsAgencyOrderDialogClose = function() {        
         $scope.isAgencyOrder = false;
    }
    
    
    $scope.agencyOrderConfirm = function(){
        $rootScope.orderType='ort';
        $scope.checkPassportData();
        $scope.isAgencyOrder = false;
    }      
    
    
    $rootScope.getOrder = function(){
        return $scope.order;
    }      
    
    
    $scope.getRoomsData = function(type){
        var rooms = $rootScope.getSelectedRoomsForHotel($rootScope.selectedHotelIndex);    
        var result = '';
        for(var i=1; i <= rooms.length; i++){
            switch(type){
                case 'meal':
                    result += ((i-1)?',':'')+rooms[i-1]['mealname'];
                break;    
                case 'name':
                    result += ((i-1)?',':'')+rooms[i-1]['roomname'];
                break;
                case 'content':
                    result += ((i-1)?',':'') +'1ном.('+ $rootScope.getRoomAdultChildren(i-1)+')';
                break;
                case 'quantity':
                    if(result === '') result = [];
                    result.push(1);
                break;    
            }
        }
        return result;
    }
    
    
    var getInsuranceDataAsText = function(){
        var order = $rootScope.getOrder();
        var message = '';       
        for(var i in order.people){
            if(order.people[i].insurance){
                if(!message.length) message = 'Следующим туристам требуется медицинская страховка: ';                
                else message += ', ';
                
                message += order.people[i].firstname +' '+order.people[i].lastname;
            }
        }
        if(message.length){
            var cost =  $scope.calculateInsuranceVisas($rootScope.selectedHotel.currency, 'insurance');
            message += '. Общая стоимость страховки '+cost+' '+$rootScope.selectedHotel.currency+'.';
        } 
        return message;
    }
    
    
    var getPayObject = function(){
        var order = $rootScope.getOrder();
        var rooms = $rootScope.getSelectedRoomsForHotel($rootScope.selectedHotelIndex);    
        var pay = {
            comment: order.comment,
            country: $rootScope.hotels.getvisacost[0].country,
            bonuscard: order.bonuscard,
            hotelcode: $rootScope.selectedHotel.code,
            adults: $rootScope.search['adults[]'],
            children: $rootScope.search['children[]'],
            id_agency: $rootScope.payAgencyId,
            visa_cost: $scope.calculateInsuranceVisas($rootScope.selectedHotel.currency, 'visa'),
            orderid: $scope.cancellation.orderid,
            
            'rt-LastDayWithoutPenality': $scope.cancellation.LastDayWithoutPenality,
            'rt-LastDayWithoutPenalityCurrency': $scope.cancellation.LastDayWithoutPenalityCurrency,
            'rt-LastDayWithoutPenalityFromDate': $scope.cancellation.LastDayWithoutPenalityFromDate,
            'rt-LastDayWithoutPenalityToDate': $scope.cancellation.LastDayWithoutPenalityToDate,
            'rt-LastDayWithoutPenalityType': $scope.cancellation.LastDayWithoutPenalityType,
            'rt-LastDayWithoutPenalityValue': $scope.cancellation.LastDayWithoutPenalityValue,
            'rt-charset': 'UTF-8',
            'rt-clientNationality': '',            
            'rt-cost': $rootScope.calculateHotelFlight($rootScope.selectedHotelIndex, $rootScope.selectedHotel.currency),
            'rt-cost-RUR': $rootScope.calculateHotelFlight($rootScope.selectedHotelIndex, 'RUR'),
            'rt-cost-RUR-stab': $rootScope.calculateHotelFlight($rootScope.selectedHotelIndex, 'RUR'),
            'rt-cost-select-who-order': $rootScope.calculateHotelFlight($rootScope.selectedHotelIndex, $rootScope.selectedHotel.currency),
            'rt-count-tourist': $rootScope.getPeopleQuantity(),
            'rt-course-EUR': $rootScope.hotels.courses['EUR'],
            'rt-course-GBP': $rootScope.hotels.courses['GBP'],
            'rt-course-USD': $rootScope.hotels.courses['USD'],
            'rt-from-city-id': ($filter('filter')($rootScope.cities, {key: $rootScope.search.city}))[0].key,
            'rt-from-city-name': ($filter('filter')($rootScope.cities, {key: $rootScope.search.city}))[0].name,
            'rt-from-date': $rootScope.selectedFlight.origin[0][$rootScope.selectedFlight.origin[0].length-1].ArrivalDate,
            'rt-hotel': $rootScope.selectedHotel.name,
            'rt-night': $rootScope.getDuration($rootScope.selectedFlight.origin[0][$rootScope.selectedFlight.origin[0].length-1].ArrivalDate, $rootScope.selectedFlight.origin[$rootScope.selectedFlight.origin.length-1][0].DepartureDate),
            'rt-oper-alias': $rootScope.selectedHotel.opername,
            'rt-oper-id': $rootScope.selectedHotel.oper,
            'rt-oper-name': $rootScope.selectedHotel.opername,
            'rt-valute': $rootScope.selectedHotel.currency,
            'rt-valute-select-who-order': $rootScope.selectedHotel.currency,
            'rt-who-send-brone': 'tourist',
            'rt-spo': 'HOTEL',
            'rt-star': $rootScope.selectedHotel.starname,
            'rt-to-city-id': $rootScope.search.living,
            'rt-to-city-name': $rootScope.getCityName(),
            'rt-to-country-id': $rootScope.selectedCountry.key,
            'rt-to-country-name': $rootScope.selectedCountry.name,
            'rt-to-date': $rootScope.selectedFlight.origin[$rootScope.selectedFlight.origin.length-1][0].DepartureDate,
            'rt-tokens': '[]',            
            'rt-rooms-count': '1,'+(rooms.length>1 ? '1' : '0')+','+(rooms.length>2 ? '1' : '0'),
            'rt-session-hotel-code': rooms[0].dataGateway.hotel_code,
            'rt-session-id': $rootScope.hotels.sessionid,
            'rt-room': $scope.getRoomsData('name'),
            'rt-meal': $scope.getRoomsData('meal'),
            'rt-rooms': $scope.getRoomsData('content')
        };
         
        
        var room_person_numbers;
        var current_person = 0;
        
        pay['rt-contractcode'] = {};
        pay['rt-roomcode'] = {};
        pay['rt-roomid'] = {};
        pay['rt-roomoptionid'] = {};
        pay['rt-tourist-f-eng'] = {};
        pay['rt-tourist-i-eng'] = {};
        
        for(var i=1; i <= rooms.length; i++){
            pay['rt-contractcode'][i] = rooms[i-1].dataGateway.contract_incomingOffice_code;
            pay['rt-roomcode'][i]= rooms[i-1]['roomcode'];
            pay['rt-roomid'][i] = rooms[i-1]['roomid']; 
            pay['rt-roomoptionid'][i] = rooms[i-1]['roomoptionid']; 
            room_person_numbers = $scope.getCollectionForRoom(rooms[i-1]);
            pay['rt-tourist-f-eng'][i] = {};
            pay['rt-tourist-i-eng'][i] = {};            

            for(var j=0; j < room_person_numbers[room_person_numbers.length-1]; j++){
                pay['rt-tourist-f-eng'][i][j+1] = order.people[current_person].lastname;
                pay['rt-tourist-i-eng'][i][j+1] = order.people[current_person].firstname;
                current_person++;
            }
        }
        
        
        var passport_data;
        
        pay['rt-born-pasport-deistvitelendo'] = {};
        pay['rt-born-pasport-number'] = {};
        pay['rt-born-pasport-serial'] = {};
        pay['rt-born-visa-consulstvo'] = {};
        pay['rt-tourist-born'] = {};
        pay['rt-tourist-fi-eng'] = {};
        pay['rt-tourist-fio-rus'] = {};
        pay['rt-tourist-pass'] = {};
        pay['rt-tourist-passdateto'] = {};
        pay['rt-personal-med'] = {};
        pay['rt-sex'] = {};
            
        for(i=0; i < order.people.length; i++){      
            passport_data = order.people[i]['passport'].split('-');
            pay['rt-born-pasport-deistvitelendo'][i+1] = order.people[i]['passport_date'];
            pay['rt-born-pasport-number'][i+1] = passport_data[1];
            pay['rt-born-pasport-serial'][i+1] = passport_data[0];
            pay['rt-born-visa-consulstvo'][i+1] = order.people[i].visa=='on' ? $rootScope.hotels.getvisacost[order.people[i].visacity].city : '0';

            pay['rt-tourist-born'][i+1] = order.people[i]['birth'];
            pay['rt-tourist-fi-eng'][i+1] = order.people[i].lastname+' '+order.people[i].firstname;
            pay['rt-tourist-fio-rus'][i+1] = '';

            pay['rt-tourist-pass'][i+1] = order.people[i]['passport'];
            pay['rt-tourist-passdateto'][i+1]= order.people[i]['passport_date'];
            if(order.people[i].insurance){
                pay['rt-personal-med'][i+1] = 'on';
            }
            pay['rt-sex'][i+1] = order.people[i]['gender'];
        };
        return pay;
    }       
    
    
    
});