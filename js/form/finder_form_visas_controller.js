// контроллер формы виз
tourFinderApp.controller('TourFinderVisasCtrl', function($scope, $rootScope, $http, $cookies, $filter, $timeout, $sce, Rates) {   
    $scope.countries = (typeof $rootScope.visageo == 'undefined' ? [] : $rootScope.visageo); 
    $scope.rates = Rates;     
        
    if(typeof($rootScope.visageo)== 'undefined'){    
    $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:{'action':'visasgeo'}, timeout: $rootScope.timeout}).
        success(function(response, status, headers, config){
            var result = response.data;
            $rootScope.visageo = result;
            $scope.countries = result;
        });
    }
        
    $rootScope.getVisaText = function(){
         $rootScope.visatext = false;
         var data = {
             action: 'visatext', 
             city:  $scope.search.city, 
             country:  $scope.search.country
         };
         $http.jsonp($rootScope.rootPath+'finder_index.php?callback=JSON_CALLBACK',{params:data, timeout: $rootScope.timeout}).
         success(function(result, status, headers, config){                       
            if(
                    result && 
                    typeof result.data != 'undefined' &&
                    result.data
            ) $rootScope.visatext = {url: result.data.url, text: $sce.trustAsHtml(result.data.text)};
            else $rootScope.visatext = {url: false, text: $sce.trustAsHtml('<center>Нет данных</center>')};            
         }).
         error(function(data, status, headers, config){});  
         if($rootScope.search.type == 'visas') $rootScope.view = 'visa';
     }
     
     $scope.setDefaultCity = function(){
         var cities = $filter('filter')($scope.countries, {key: $scope.search.country})[0].cities;
         if(!$filter('filter')(cities, {key: $scope.search.city}).length) $scope.search.city = cities[0].key;
     }
    
});