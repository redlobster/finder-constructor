<?php
$dir = str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__);
$dir = preg_replace('~/'.basename(__DIR__).'$~', '/', $dir);
header("Content-Type: text/javascript");  
?>
var providers = {};

var finder_form_loader = {   
    
    root_path: '<?php echo "http://".$_SERVER['HTTP_HOST'].$dir;?>',

    
    init: function(){                      
        finder_form_loader.form = $('div.findConstructor').eq(0);
        finder_form_loader.results = $('div.findConstructor').eq(1);
    },
    
    reload: function(){
        finder_form_loader.init();
        finder_form_loader.form.replaceWith('<div class="findConstructor" ng-controller="TourFinderSearchCtrl" ng-include="" src="\'finder_form\'"></div>');
        finder_form_loader.results.replaceWith('<div class="findConstructor finder_search" ng-controller="TourFinderResultsCtrl" ng-include="" src="\'finder_results\'"></div>');
        finder_form_loader.init();
    },
    
    providersCall: function(){
        finder_form_loader.reload();
                           
        var queue = angular.module('tourFinderApp')._invokeQueue;
        for(var i=0;i<queue.length;i++) {
             var call = queue[i];            
             var provider = providers[call[0]];
             if(provider) {
                 provider[call[1]].apply(provider, call[2]);
             }
         }

         $('body').injector().invoke(function($compile, $rootScope) {
             $compile(finder_form_loader.form)($rootScope);
             $compile(finder_form_loader.results)($rootScope);
             $rootScope.$apply();
         });
    },
            
    load: function(){                
        finder_form_loader.reload();
        //css    
        var url = finder_form_loader.root_path + 'finder_index.php?action=css';

        if(!$('#findConstructorCss').length){
            $('head').append('<link id="findConstructorCss" rel="stylesheet" type="text/css" href="'+url+'">');            
            $.getScript('http://rt.ross-tur.ru/spo/widget.js'); 
        }


        $.getJSON(finder_form_loader.root_path+'finder_index.php?action=templates&callback=?', null, function(templates) {
            templates = templates.data;
            for(var tpl in templates){
                if(!$('#'+tpl).length) $('body').append('<script type="text/ng-template" id="'+tpl+'">'+templates[tpl]+'</script>');
            }                        

            if(typeof angular == 'undefined'){                
                    $.ajax({
                        url: finder_form_loader.root_path+'finder_index.php?action=js',
                        dataType: "script",
                        cache: true,
                        success: function(script, textStatus, jqXHR){ 

                            try{
                                angular.bootstrap($('body'),['tourFinderApp']);                                       
                            }
                            catch(e){

                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log('error: ' + errorThrown);
                        }        
                    });         
            }
            else{
                if(
                    typeof gde != 'undefined' && 
                    typeof gde.finder != 'undefined' && 
                    typeof gde.finder.loading != 'undefined'
                ) gde.finder.loading(0);                    
                finder_form_loader.providersCall();
            }
        });                  
    }
}


$().ready(function(){
    if(typeof google == 'undefined'){
        $.getScript('http://maps.googleapis.com/maps/api/js?sensor=false&callback=finder_form_loader.load'); 
    }
    else finder_form_loader.load();
});