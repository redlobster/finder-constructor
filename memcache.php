<?php

/*
 * Class to access memcached vars and set them.
 * @author RedLobster
 */

namespace Finder;


class MemcacheHandler{
    
    public function __construct($server = 'localhost', $port = 11211) {
        $this->memcache = new \Memcache;
        $this->errors = array();
        if(!$this->memcache->connect($server, $port)){
                $this->errors[] = "Could not connect";
        }       
    }
    
    
    public function set($key, $value, $seconds = 900){
        if(!$this->memcache->set($key, $value, false, $seconds)){
            $this->errors[] = "Failed to save data at the server";
            return false;
        }
        return true;
    }
    
    
    public function get($key){
            try{
                $data = $this->memcache->get($key);
                return $data;
            }
            catch(Exception $e){ 
                $this->errors[] = "Failed to get data from the server";
                return false;
            }        
    }
    
    
    public function getVersion(){
        return $this->memcache->getVersion();        
    }    
}
?>
